'use strict';
// How many many pairs of reversed matched strings are in a string 
// Don't it works correctly
function recursiveCounter(array, left, right, counter, initialLeft, initialRight) {

    console.log(left, right, array);
    if (left >= right) {
        return counter;
    } else if (array[left] === array[right]) {
        counter++;
        console.log('+1\n');
        return recursiveCounter(array, left + 1, right - 1, counter, initialLeft, initialRight);
    } else if (array[left] !== array[right]) {
        return recursiveCounter(array, initialLeft, initialRight - 1, counter, initialLeft, initialRight - 1);
    }

}
let counter = 0;
let string = 'ababa'.split("");
for (let i = 0; i < string.length; i++) {
    // console.log(i, j);
    console.log(string[i], string[string.length - 1]);
    counter += recursiveCounter(string, i, string.length - 1, 0, i, string.length - 1);
}

// counter = recursiveCounter('abbaba'.split(""), 0, 'abbaba'.length - 1, counter);
console.log(`Counter is ${counter}`);