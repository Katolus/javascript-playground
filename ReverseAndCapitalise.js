function reverseAndCapitalize(sentenceIn) {
    console.log(sentenceIn);
    const aAlphabet = 'abcdefghijklmnopqrstuvwxyz'.split("");
    const aAllowedPunctuation = ['!', ',', '.', '?'];
    let newSentence = [];

    function reverse(word) {
        let currentChar;
        let newWord = [];
        let isCapitalized = false;
        const wordLength = word.length;
        for (let i = 1; i <= wordLength; i++) {
            currentChar = word[wordLength - i].toLowerCase();
            console.log(currentChar);
            if (!isCapitalized) {
                if (aAlphabet.includes(currentChar)) {
                    // console.log("Letter is a part of the alphabet");
                    newWord.push(currentChar.toUpperCase());
                    isCapitalized = true;
                    continue;
                }
                else if (!aAllowedPunctuation.includes(currentChar)) {
                    continue;
                }
            }
            newWord.push(currentChar);
        }
        return newWord.join("");
    }



    const aSplitSentence = sentenceIn.split(" ");

    // for (let word of aSplitSentence) {
    //     newSentence.push(reverse(word));
    // }


    let aReversedSentence = aSplitSentence.map((word, index) => {
        return reverse(word);
    })
        .reverse();
    // .map((word, index, reversedSentence) => {
    //     let newWord;
    //     const puncArray = word.toLowerCase().split("").filter(char => aAllowedPunctuation.includes(char));
    //     index === 0 ? newWord = reversedSentence[index] : newWord = reversedSentence[index - 1].concat(...puncArray);
    //     return newWord;
    // });
    let currentWord;
    for (let i = 0; i < aReversedSentence.length - 1; i++) {
        currentWord = aReversedSentence[i];
        punctuationArray = currentWord.split("").filter(char => aAllowedPunctuation.includes(char));
        if (punctuationArray.length > 0 && i === 0) {
            currentWord = [...punctuationArray, currentWord];
        } else {
            aReversedSentence[i - 1] = [aReversedSentence[i - 1], ...punctuationArray].join("");
            currentWord = currentWord.split("").filter(char => !aAllowedPunctuation.includes(char));
        }
        aReversedSentence[i] = currentWord.join("");
        // console.log(currentWord);
    }
    console.log(aReversedSentence);
    return aReversedSentence.join(" ");

}

console.log(reverseAndCapitalize("Hey there, Alan"));
console.log(reverseAndCapitalize("Morning? I ?, Sup Alan"));