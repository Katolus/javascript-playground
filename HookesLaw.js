// spring force ~ kd

const spring = function () {
    let stiffness = 1;
    let mass = 5; // kg
    let springLength = 0; // m
    return {
        setStiffnes: function (newStiffnes) {
            stiffness = newStiffnes;
        },
        setMass: function (newMass) {
            mass = newMass;
        },
        setSpringLength: function (newSpringLength) {
            springLength = newSpringLength;
        },
    }
}