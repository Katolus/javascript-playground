function sockMerchant(n, ar) {
    let numberOfMatchingPairs = 0;

    function getUniqueItemsWithFilterArray() {

        return ar.filter((item, index) => {
            return ar.indexOf(item) == index;
        });
    }

    function getUniqueItemsWithObject() {
        var itemsSeen = {};
        return ar.filter((item, index) => {
            return itemsSeen.hasOwnProperty(item) ? false : itemsSeen[item] = true;
        });
    }

    function getUniquesCount() {
        var itemsSeen = {};
        ar.map((item, index) => {
            itemsSeen.hasOwnProperty(item) ? ++itemsSeen[item] : itemsSeen[item] = 1;
        });

        let array = [];
        for (let key in itemsSeen) {
            let sock = {};
            numberOfMatchingPairs = numberOfMatchingPairs + Math.floor(itemsSeen[key]/2);
            sock[key] = itemsSeen[key];
            array.push(sock);
        }

        return array;
    }

    console.time("SimpleFilter");
    console.log(getUniqueItemsWithFilterArray());
    console.timeEnd("SimpleFilter");

    console.time("SimpleFilter");
    console.log(getUniqueItemsWithFilterArray());
    console.timeEnd("SimpleFilter");

    console.time("SimpleObjectFilter");
    console.log(getUniqueItemsWithObject());
    console.timeEnd("SimpleObjectFilter");

    console.time("Count");
    console.log(getUniquesCount());
    console.timeEnd("Count");

    return numberOfMatchingPairs;
}

console.log(sockMerchant(9, [10, 20, 20, 10, 10, 30, 50, 10, 20]));