var fs = require('fs');
const https = require('https');

const getData = function (fileName, url) {
    return new Promise(function (resolve, reject) {
        const req = https.get(url, function (res) {
            res.setEncoding('UTF8');
            res.on('data', function (data) {
                fs.writeFile(`data/${fileName}.txt`, data, function (err) {
                    if (err) throw err;
                    console.log('Saved!');
                });
                resolve(fileName);
            });
        });

        req.on('error', (e) => {
            reject(e);
        });
    });
}

const printResult = (val) => console.log(`Initialized test ${val}`);

function getAndSaveTestData(aUrl) {

    getData('variables', aUrl.variables).then(function (data) {
        printResult(data);
        return getData('expected', aUrl.expected);
    }).then(function (data) {
        printResult(data);
    }).catch(function (err) {
        console.error(err);
    });
}

module.exports = { getAndSaveTestData };