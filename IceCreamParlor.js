'use strict';
var assert = require("assert");


function whatFlavors(cost, money) {
    let arrayOfRest = [];
    let rest;
    let restIndex;
    for (let i = 0; i < cost.length; i++) {
        rest = money - cost[i];
        restIndex = arrayOfRest.indexOf(cost[i]);
        if (restIndex > -1) { // If the rest is in the array
            return [i + 1, restIndex + 1].sort();
            // return [restIndex, i].sort();
        }
        arrayOfRest.push(rest);
        // arrayOfRest[i] = rest;
    }
    return [];
}
function whatFlavors2(cost, money) {
    let rests = {};
    let rest;
    // let restIndex;
    for (let i = 1; i < cost.length + 1; i++) {
        // Check if there was a match
        if (rests[cost[i - 1]]) {
            return [rests[cost[i - 1]], i].sort();
        }
        rest = money - cost[i - 1];
        // If not match and rest is considerable
        if (rest > 0) {
            rests[rest] = i;
        }
        // console.log(rests);
    }
    return [];
}
function whatFlavors3(cost, money) {
    let rests = {};
    let rest;
    // let restIndex;
    for (let i = 1; i < cost.length + 1; i++) {
        // Check if there was a match
        if (rests[cost[i - 1]]) {
            console.log([rests[cost[i - 1]], i].sort((a, b) => a - b).join(" "));
            delete rests[cost[i - 1]];
            continue;
        }
        rest = money - cost[i - 1];
        // If not match and rest is considerable
        if (rest > 0) {
            rests[rest] = i;
        }
        // console.log(rests);
    }
    return [];
}

// whatFlavors3(...[[3, 2, 2, 1], 4])
whatFlavors3(...[
    [23, 24, 2, 22, 2, 15, 5, 52, 3, 1],
    25
])
// let testFunction = whatFlavors2;
// describe("Testing whatFlavors", function () {
//     const tests = [
//         {
//             variables: [[1, 4, 5, 3, 2], 4],
//             expected: [1, 4]
//         },
//         {
//             variables: [[2, 2, 4, 3], 4],
//             expected: [1, 2]
//         }, {
//             variables: [[1, 1, 1, 1], 4],
//             expected: []
//         }, {
//             variables: [[3, 2, 2, 1], 4],
//             expected: [1, 4]
//         }, {
//             variables: [[], 4],
//             expected: []
//         }
//     ];
//     tests.forEach(test => {
//         it(`${test.variables}`, function () {
//             assert.deepEqual(testFunction(...test.variables), test.expected);
//         });
//     });
// });