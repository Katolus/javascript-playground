'use strict';
var assert = require('assert');

function sherlockAndAnagrams(s) {
    let counter = 0;
    let aS = s.split("");
    let subWords = {};
    let range;

    // Create all possible words
    for (let j = aS.length; j >= 1; j--) {

        range = [0, j];
        subWords[range[1] - range[0]] = [];
        while (range[1] < aS.length + 1) {
            // subWords.push(aS.slice(range[0], range[1]));
            subWords[range[1] - range[0]].push(aS.slice(range[0], range[1]));
            range[0]++;
            range[1]++;
        }
    }
    console.log('Creating words finished');

    // Iterate the subWord array
    let word;
    let subWordsGroup;
    for (let wordGroup in subWords) {
        subWordsGroup = subWords[wordGroup];
        // console.log(...subWordsGroup);
        for (let j = 0; j < subWordsGroup.length; j++) {
            word = subWordsGroup[j];
            for (let i = 0; i < subWordsGroup.length; i++) {
                if (j === i) {
                    continue;
                }
                if (compareValuesInTwoArrays(word, subWordsGroup[i])) {
                    counter++;
                };
            }
        }
    }
    return counter;

}

function compareTwoArrays(a1, a2) {
    if (a1.length !== a2.length) {
        return false;
    }
    for (let i = 0; i < a1.length; i++) {
        if (a1[i] !== a2[i]) {
            return false;
        };
    }
    return true;
}

Array.prototype.myIndexOf = function (char, arrayOfExcludedIndexes) {
    for (let i = 0; i < this.length; i++) {
        if (arrayOfExcludedIndexes.indexOf(i) > -1) {
            continue;
        }
        if (this[i] === char) {
            return i;
        }
    }
    return -1;
}

function compareValuesInTwoArrays(a1, a2) {
    if (a1.length !== a2.length) {
        return false;
    }

    let foundIndexes = [];
    let currentIndex;
    for (let i = 0; i < a1.length; i++) {

        currentIndex = a2.myIndexOf(a1[i], foundIndexes);

        if (currentIndex > -1) {
            foundIndexes.push(currentIndex);
        } else {
            return false;
        }

    }
    console.log(`Array with same set of values: ${a1} and ${a2}`);
    return true;
}

function findArrayWithSameSetOfChar(searchedArray, setOfArrays) {
    let counter = 0;
    // Array are the same length

    for (let array of setOfArrays) {

        if (compareValuesInTwoArrays(array, searchedArray)) {
            counter++;
        };
    }
    // Minus 1 because it will find itself
    return counter - 1;
}


let testing = sherlockAndAnagrams;

describe('Anagram tests', function () {
    this.timeout(5000);
    const tests = [
        {
            variables: 'abba',
            expected: 4
        }, {
            variables: 'abcd',
            expected: 0
            // }, {
            //     variables: 'ifailuhkqqhucpoltgtyovarjsnrbfpvmupwjjjfiwwhrlkpekxxnebfrwibylcvkfealgonjkzwlyfhhkefuvgndgdnbelgruel',
            //     expected: 399
            // }, {
            //     variables: 'gffryqktmwocejbxfidpjfgrrkpowoxwggxaknmltjcpazgtnakcfcogzatyskqjyorcftwxjrtgayvllutrjxpbzggjxbmxpnde',
            //     expected: 471
            // }, {
            //     variables: 'mqmtjwxaaaxklheghvqcyhaaegtlyntxmoluqlzvuzgkwhkkfpwarkckansgabfclzgnumdrojexnrdunivxqjzfbzsodycnsnmw',
            //     expected: 370
            // }, {
            //     variables: 'ofeqjnqnxwidhbuxxhfwargwkikjqwyghpsygjxyrarcoacwnhxyqlrviikfuiuotifznqmzpjrxycnqktkryutpqvbgbgthfges',
            //     expected: 403
            // }, {
            //     variables: 'zjekimenscyiamnwlpxytkndjsygifmqlqibxxqlauxamfviftquntvkwppxrzuncyenacfivtigvfsadtlytzymuwvpntngkyhw',
            //     expected: 428
        }
    ];

    tests.forEach(test => {
        it('Test: ' + test.variables, function () {

            assert.equal(testing(test.variables), test.expected);
        })
    })
});