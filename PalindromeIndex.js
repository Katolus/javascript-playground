// Palindrome - a word, number, phrase or other sequence that reads the same backward as forward

// Q: Find a character to remove for a variable to become a palindrome
'use strict';
var assert = require("assert");

function palindromeIndex(s) {

    // If the word is a palindrome or no such character exists, print -1
    // previousIteration = []

    const sS = s.length - 1;
    for (let i = 0, j = sS; i <= j;) {
        // console.log(s[i], s[j]);
        if (s[i] === s[j]) {
            i++;
            j--;
            continue;
        } else {
            let newArray = s.slice(0, i) + s.slice(i + 1);
            if (isPalindrome(newArray)) {
                return i;
            }else { 
                newArray = s.slice(0, j) + s.slice(j + 1);
            }
            if(isPalindrome(newArray)){
                return j;
            }else{ 
                return -1; 
            }
            // switch (checkNext(s, i, j)) {
            //     case "i":
            //         return i;
            //     case "j":
            //         return j;
            //     default:
            //         return -1;
            // }
        }
    }
    return -1;
}

function isPalindrome(s) {
    const sS = s.length - 1;
    for (let i = 0, j = sS; i <= j;) {
        // console.log(s[i], s[j]);
        if (s[i] === s[j]) {
            i++;
            j--;
            continue;
        }else{
            return false;
        }
    }
    return true;
}
function checkNext(s, i, j) {
    if (s[i] !== s[j - 1]) {
        console.log(`Returning i because ${s[i]} !== ${s[j - 1]}`);
        return 'i';
    } else if ((s[i + 1] !== s[j])) {
        console.log(`Returning i because ${s[i]} !== ${s[j - 1]}. Indexes = ${[i, j - 1]} `);
        console.log(`Returning j because s[i + 1]= ${s[i + 1]} !== s[j] = ${s[j]}. Indexes = ${[i + 1, j]}`);
        return 'j';
    } else {
        return checkNext(s, i + 1, j + 1);
    }

}
function getRemoveChar(array, i, j) {
    if (i > j) { // 1 3
        return true;
    } else if (array[i] !== array[j]) {
        return false;
    } else {
        getRemoveChar(array, i + 1, j + 1);
    }
}

let testingFunction = palindromeIndex

describe("Palindrome Index function: ", function () {

    const tests = [{
        variables: 'aaab',
        expected: 3
    }, {
        variables: 'baa',
        expected: 0
    }, {
        variables: 'aaa',
        expected: -1
    }, {
        variables: 'quyjjdcgsvvsgcdjjyq',
        expected: 1
        }, {
            variables: 'hgygsvlfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh',
            expected: 8
        }, {
            variables: 'fgnfnidynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf',
            expected: 33
        }, {
            variables: 'bsyhvwfuesumsehmytqioswvpcbxyolapfywdxeacyuruybhbwxjmrrmjxwbhbyuruycaexdwyfpaloyxbcpwsoiqtymhesmuseufwvhysb',
            expected: 23
        }, {
            variables: 'fvyqxqxynewuebtcuqdwyetyqqisappmunmnldmkttkmdlnmnumppasiqyteywdquctbeuwenyxqxqyvf',
            expected: 25
        }, {
            variables: 'mmbiefhflbeckaecprwfgmqlydfroxrblulpasumubqhhbvlqpixvvxipqlvbhqbumusaplulbrxorfdylqmgfwrpceakceblfhfeibmm',
            expected: 44
        }, {
            variables: 'tpqknkmbgasitnwqrqasvolmevkasccsakvemlosaqrqwntisagbmknkqpt',
            expected: 20
        }, {
            variables: 'lhrxvssvxrhl',
            expected: -1
        }, {
            variables: 'prcoitfiptvcxrvoalqmfpnqyhrubxspplrftomfehbbhefmotfrlppsxburhyqnpfmqlaorxcvtpiftiocrp',
            expected: 14
        }, {
            variables: 'kjowoemiduaaxasnqghxbxkiccikxbxhgqnsaxaaudimeowojk',
            expected: -1
        }, {
            variables: 'hgygsvlfcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcwflvsgygh',
            expected: 44
    }];

    tests.forEach(test => {
        it("Test variables: " + test.variables, function () {
            assert.equal(testingFunction(test.variables), test.expected);
        })
    })
});

