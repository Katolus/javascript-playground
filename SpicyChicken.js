function spicyChicken(min, max) {
    let message;
    // To save memory
    // const if_divided_by_3 = "Spicy";
    // const if_divided_by_5 = "Chicken";
    // const if_divided_by_15 = "!";
    for (let i = min; i <= max; i++) {
        message = [];
        message.push(i + " ");
        if (i % 3 === 0) {
            message.push("Spicy");
        }
        if (i % 5 === 0) {
            message.push("Chicken");
        }
        if (i % 15 === 0) {
            message.push("!");
        }
        console.log(message.join(""));
    }
}

spicyChicken(1, 100);