(function getUniqueKeys() {
  let map = {};

  const temp = {
    "PAYEVNT": {
        "Rp": {
            "SoftwareInformationBusinessManagementSystemId": "str1234",
            "AustralianBusinessNumberId": "str1234",
            "WithholdingPayerNumberId": "str1234",
            "OrganisationDetailsOrganisationBranchC": "str",
            "OrganisationName": {
                "DetailsOrganisationalNameT": "str1234",
                "PersonUnstructuredNameFullNameT": "str1234"
            },
            "ElectronicContact": {
                "ElectronicMailAddressT": "str1234",
                "TelephoneMinimalN": "str1234"
            },
            "AddressDetailsPostal": {
                "Line1T": "str1234",
                "Line2T": "str1234",
                "LocalityNameT": "str1234",
                "StateOrTerritoryC": "AAT",
                "PostcodeT": "str1234",
                "CountryC": "ad"
            },
            "Payroll": {
                "PaymentRecordTransactionD": "2012-12-13",
                "InteractionRecordCt": "33",
                "MessageTimestampGenerationDt": "2012-12-13T12:12:12",
                "InteractionTransactionId": "str1234",
                "AmendmentI": "true",
                "IncomeTaxAndRemuneration": {
                    "PayAsYouGoWithholdingTaxWithheldA": "123.45",
                    "TotalGrossPaymentsWithholdingA": "123.45"
                }
            },
            "Declaration": {
                "SignatoryIdentifierT": "str1234",
                "SignatureD": "2012-12-13",
                "StatementAcceptedI": "true"
            }
        },
        "Int": {
            "AustralianBusinessNumberId": "str1234",
            "TaxAgentNumberId": "str1234",
            "PersonUnstructuredNameFullNameT": "str1234",
            "ElectronicContact": {
                "ElectronicMailAddressT": "str1234",
                "TelephoneMinimalN": "str1234"
            },
            "Declaration": {
                "SignatoryIdentifierT": "str1234",
                "SignatureD": "2012-12-13",
                "StatementAcceptedI": "true"
            }
        }
    }
};

  function recursive(object) {
    for (let innerObject in object) {
      if (typeof object[innerObject] === 'object') {
        _addToMap(innerObject);
        recursive(object[innerObject]);
      } else if (Array.isArray(object[innerObject])) {
        for (let innerArrayItem of object[innerObject]) {
          if (typeof object[innerArrayItem] === 'object') {
            _addToMap(innerArrayItem);
            recursive(object[innerArrayItem]);
          } else {
            _addToMap(innerArrayItem);
          }
        }
      } else {
        _addToMap(innerObject);
      }
    }
  }

  function _addToMap(sKey) {
    if (!map[sKey]) {
      map[sKey] = 1;
    } else {
      map[sKey] = map[sKey] + 1;
    }
  }

  recursive(temp);
  console.log(map);
})();