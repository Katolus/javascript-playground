// Complete the rotLeft function below.
function myRotLeft(a, d) {
    let array = d;
    for (let i = 0; i < a; i++) {
        array = array.map((item, index, oarray) => {
            return index === oarray.length - 1 ? oarray[0] : oarray[index + 1];
        });
    }
    return array;
}

function communityRotLeft(d,a){
    // Accroding  to tests doesn't timeout
    return a.splice(d).concat(a);
}

function main() {
    const a = "5 3";
    const d = "1 2 3 4 5";
    console.log(myRotLeft(a.split(' ')[1], d.split(' ')));
}

main();