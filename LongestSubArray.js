'use strict';
var assert = require("assert");

// let numberOfSubArraysLengths = [];
/**
 * No reordering of 'a' array or subarray
 * @param {The array of integers} a 
 * @param {Number to which sum must be less or equal} k 
 */
function maxLength(a, k) {

    // Write your code here

    if (a.length === 0) {
        return 0;
    }
    let counter = 0;
    let maxCounter = 0;
    for (let i = 0; i < a.length; i++) {
        let localSum = 0;
        let localSumCounter = 0;
        for (let j = i; j < a.length; j++) {
            localSum += a[j];
            if (localSum > k) {
                break;
            } else {
                localSumCounter++;
            }
            counter++;
        }
        if (localSumCounter > maxCounter) {
            maxCounter = localSumCounter;
        }
        if (maxCounter > a.length - i) {
            break;
        }
    }
    console.log(counter);
    return maxCounter;
}

// console.log(numberOfSubArraysLengths);
// numberOfSubArraysLengths.push(localSumCounter);
// return Math.max(...numberOfSubArraysLengths);
//     // console.log(`Just to make sure function with array order gives number ${toMakeItSure(a,k)}`);
//     console.log(`Just to make sure function with array order gives number ${toMakeSurePermutation(a, k)}`);
//     return toMakeSurePermutation(a, k);

//     let sum = 0;
//     let counter = 0;
//     let exclude = [];

//     for (let i = 0; i < a.length; i++) {
//         let result = findLowAndExclude(a, exclude);
//         sum += result;
//         // console.log(result, sum);
//         if (!(sum <= k)) {
//             break;
//         } else {
//             counter++;
//         }
//     }
//     // console.log(exclude.map(index => a[index]));
//     // console.log(`Sum of excluded elements is ${exclude.slice(0, -1).reduce((acc, index) => acc += a[index])} <= ${k} at counter = ${counter}`);
//     // console.log(exclude.sort((a, b) => a - b));
//     // console.log(`Excluded length = ${exclude.sort((a, b) => a - b).length}`);

//     // After testing it hard to believe that result 46 for test case number 3 is the correct result

//     return counter;
// }

function toMakeItSure(a, k) {
    let sum = 0;
    let counter = 0;
    let newArray = a.sort((a, b) => a - b);
    for (let i = 0; i < newArray.length; i++) {
        sum += newArray[i];
        if (sum > k) {
            break;
        } else {
            counter++;
        }
    }
    return counter;
}

function toMakeSurePermutation(a, k) {

    let numberOfSubArraysLengths = [];
    for (let i = 0; i < a.length; i++) {
        let localSum = 0;
        let localSumCounter = 0;
        for (let j = i; j < a.length; j++) {
            localSum += a[j];
            if (localSum > k) {
                break;
            } else {
                localSumCounter++;
            }
        }
        numberOfSubArraysLengths.push(localSumCounter);
    }
    // console.log(numberOfSubArraysLengths);
    return Math.max(...numberOfSubArraysLengths);
}

function findLowAndExclude(array, aOfExcludedIndex) {
    let minValue = Number.MAX_SAFE_INTEGER;
    let minValueIndex;
    for (let i = 0; i < array.length; i++) {
        if (array[i] < 0) {
            throw 'Value cannot be < 0';
        }
        if (!aOfExcludedIndex.includes(i)) {
            if (array[i] < minValue) {
                minValueIndex = i;
                minValue = array[i];
            }
        }
    }
    aOfExcludedIndex.push(minValueIndex);
    return minValue;
}

let testFunction = maxLength;

describe("Abbreviation:", function () {

    const tests = [{
        variables: [[1, 2, 3], 4],
        expected: 2
    }, {
        variables: [[3, 1, 2, 1], 4],
        expected: 3
    }, {
        variables: [[74, 659, 931, 273, 545, 879, 924, 710, 441, 166, 493, 43, 988, 504, 328, 730, 841, 613, 304, 170, 710, 158, 561, 934, 100, 279, 817,
            336, 98, 827, 513, 268, 811, 634, 980, 150, 580, 822, 968, 673, 394, 337, 486, 746, 229, 92, 195, 358, 2, 154, 709, 945, 669, 491, 125, 197, 531, 904, 723, 667, 550],
            22337],
        expected: 46
    }];

    tests.forEach(test => {
        it("Test variables: " + test.variables, function () {
            assert.equal(testFunction(...test.variables), test.expected);
        })
    });
});