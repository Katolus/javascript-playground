var assert = require('assert');

function regexVar(testString) {

     const re = /^(Mr|Mrs|Ms|Dr|Er)\.[A-Za-z]+$/;
    //  console.log(re.exec(testString));

    return re.test(testString);
}

testingFunction = regexVar;

describe('Bitwise operation', function () {

    const tests = [{
        variables: 'Mr.X',
        expected: true
    }, {
        variables: 'Mrs.Y',
        expected: true
    }, {
        variables: 'Dr#Joseph',
        expected: false
    }, {
        variables: 'Sr.Abc',
        expected: false
    }, {
        variables: 'Er.Abc',
        expected: true
    }, {
        variables: 'Er .Abc',
        expected: false
    }
    ];

    tests.forEach(test => {
        it('Variables: ' + test.variables, function () {
            assert.equal(testingFunction(test.variables), test.expected);
        });
    });
});

