function DynamicArray() {
    this.lastAnswer = 0;
    this.s1 = [];
    this.s2 = [];
    this.query = function (typeOfQuery, x, value, N, queries) {
        this.getArray = function () {
            if (((x ^ this.lastAnswer) % 2) === 0) {
                return this.s1;
            } else {
                return this.s2;
            }
        };
        this.query1 = function () {
            // XOR operator
            this.getArray().push(value);
        };
        this.query2 = function () {
            const currentSeq = queries[(x ^ this.lastAnswer) % N];
            // console.log(currentSeq);
            this.lastAnswer = this.getArray()[value % currentSeq.length];
            console.log( this.lastAnswer);
        };
        if (typeOfQuery === 1) {
            this.query1();
        } else if (typeOfQuery) {
            this.query2();
        } else {
            throw "Wrong type of query!";
        }
    };
}

function main(N, queries) {
    const dynamicArray = new DynamicArray();
    queries.map((query, index, oarray) => dynamicArray.query(...query, N, oarray));
    // console.log(dynamicArray.s1, dynamicArray.s2);
}

main(2, [
    [1, 0, 5],
    [1, 1, 7],
    [1, 0, 3],
    [2, 1, 0],
    [2, 1, 1]
]);