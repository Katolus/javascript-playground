'use strict';
var assert = require("assert");
var fs = require('fs');
const wf = require("./WriteToFile");

function isBalanced(s) {
    let bracketArray = s.split("");
    const leftBrackets = ['{', '[', '('];
    const rightBrackets = ['}', ']', ')'];
    let stack = function () {
        let stack = [];
        return {
            add: function (element) {
                stack.push(element);
            },
            remove: function () {
                return stack.pop();
            },
            size: function () {
                return stack.length;
            }
        }
    };
    const newStack = new stack();
    for (let element of bracketArray) {
        if (leftBrackets.indexOf(element) > -1) {
            newStack.add(element);
        } else if (rightBrackets.indexOf(element) > -1) {
            let lastElement = newStack.remove();
            if (lastElement !== leftBrackets[rightBrackets.indexOf(element)]) {
                return 'NO'
                // break;
            }
        }
    }
    if (newStack.size() > 0) {
        return 'NO';
    } else {
        return 'YES';
    }
}

// isBalanced('{{}(');
let testFunction = isBalanced;
describe("Testing Balanced Brackets", function () {
    const tests = [{
        variables: '{[()]}',
        expected: 'YES'
    }, {
        variables: '{ [(]) }',
        expected: 'NO'
    }, {
        variables: ')), ]], }}',
        expected: 'NO'
    },
    {
        variables: '{ { [[(())]] } }',
        expected: 'YES'
    },
    {
        variables: '}(]}){',
        expected: 'NO'
    },
    {
        variables: '(])[{{{][)[)])(]){(}))[{(})][[{)(}){[(]})[[{}(])}({)(}[[()}{}}]{}{}}()}{({}](]{{[}}(([{]',
        expected: 'NO'
    },
    {
        variables: '[()][{}[{}[{}]]][]{}[]{}[]{{}({}(){({{}{}[([[]][[]])()]})({}{{}})})}',
        expected: 'YES'
    }

    ];
    tests.forEach(test => {
        it(`${test.variables}`, function () {
            assert.equal(testFunction(test.variables), test.expected);
        });
    });
});

// describe('something slow', function () {
//     // this.slow(10000);

//     it('should take long enough for me to go make a sandwich', function () {
//         // ...
//     });

//     it('should take less than 500ms', function (done) {
//         this.timeout(500);
//         setTimeout(done, 300);
//     });
//     it('should take less than 500ms', function (done) {
//         setTimeout(done, 500);
//     });

// });


// describe('a suite of tests', function () {
//     this.timeout(500);

//     it('should take less than 500ms', function (done) {
//         setTimeout(done, 300);
//     });

//     it('should take less than 500ms as well', function (done) {
//         setTimeout(done, 250);
//     });

//     it('should take less than 500ms as well', function (done) {
//         setTimeout(done, 500);
//     });
// });

describe('Fetching test data and results', async function () {

    const aURL = {
        variables: 'https://hr-testcases-us-east-1.s3.amazonaws.com/15095/input04.txt?AWSAccessKeyId=AKIAJ4WZFDFQTZRGO3QA&Expires=1537944406&Signature=xh%2FxKREnR27lqo9q7K9OZf10l00%3D&response-content-type=text%2Fplain',
        expected: 'https://hr-testcases-us-east-1.s3.amazonaws.com/15095/output04.txt?AWSAccessKeyId=AKIAJ4WZFDFQTZRGO3QA&Expires=1537951153&Signature=rHN9rFZTH88aw7X5r%2Bp%2BxADc82I%3D&response-content-type=text%2Fplain'
    }

    const path = 'data';
    if (!fs.existsSync(`${path}/variables.txt`) || !fs.existsSync(`${path}/expected.txt`)) {
        await wf.getAndSaveTestData(aURL);
    }
    const variables = fs.readFileSync(`${path}/variables.txt`, 'utf8').split('\n').slice(1);
    const expected = fs.readFileSync(`${path}/expected.txt`, 'utf8').split('\n');

    variables.forEach((test, index) => {
        it(`${test}`, function () {
            assert.equal(testFunction(test), expected[index]);
        });
    });
})
