var assert = require('assert');

var isEven = function(n) {
    return n % 2 === 0;
};

var isOdd = function(n) {
    return !isEven(n);
};

var power = function(x, n) {
    console.log("Computing " + x + " raised to power " + n + ".");
    if(n === 0){
        return 1;
    }else if(n < 0){
        return 1 / power(x, -n);
    }else if(isOdd(n)){
        return x*power(x, n - 1);
    }else if(isEven(n)){
        var half = power(x, n/2);
        return half*half;
    }
};

var displayPower = function(x, n) {
    console.log(x + " to the " + n + " is " + power(x, n));
};
// displayPower(3,-1);
// displayPower(3, 0);
assert.equal(power(3, 0), 1);
// displayPower(3, 1);
assert.equal(power(3, 1), 3);
// displayPower(3, 2);
assert.equal(power(3, 2), 9);
// displayPower(3, -1);
assert.equal(power(3, -1), 1/3);
assert.equal(power(3, -4), 1/81);
assert.equal(power(2, -5), 1/32);
