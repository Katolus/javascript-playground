var assert = require('assert');

function repeatedString(s, n) {
    let counter = 0;
    const letter = 'a';
    const numberOfRepetitions = Math.ceil(n / s.length);
    let newWord = [];
    for (let i = 0; i < numberOfRepetitions; i++) {
        newWord.push(s);
    }
    const charString = newWord.join("").split("");
    for (let i = 0; i < n; i++) {
        if (charString[i] === letter) {
            counter++;
        }
    }
    return counter;
}

function repeatedString2(s, n) {
    let counter = 0;
    const letter = 'a';
    const numberOfRepetitions = Math.floor(n / s.length);
    const lastIndex = n % s.length;
    const charString = s.split("");
    for (let i = 0; i < charString.length; i++) {
        if (charString[i] === letter) {
            counter++;
        }
    }
    return counter * numberOfRepetitions + charString.slice(0, lastIndex).filter(char => char === letter).length;
}
repeatedString2(...['aba', 10]);
let testingFunction = repeatedString2;
describe('Testing function', function () {
    var tests = [{
        testVar: ['aba', 10],
        expected: 7
    }, {
        testVar: ['a', 1000000000000],
        expected: 1000000000000
    }];

    tests.forEach(test => {
        it(testingFunction.name + ': ' + test.testVar, function () {
            assert.equal(testingFunction(...test.testVar), test.expected);
        });
    });
});