var assert = require('assert');

function getMaxLessThanK2(n, k) {

    let array = [];
    // Initialize array size 
    for (let i = 1; i <= n; i++) {
        array.push(i);
    }

    let maxAndBitValue = 0;
    let topValue;
    let innerValue;
    let andResult;
    for (let i = 0; i < array.length; i++) {
        topValue = array[i];
        for (let j = i + 1; j < array.length; j++) {
            innerValue = array[j];
            andResult = topValue & innerValue;
            if (andResult > maxAndBitValue && andResult < k) {
                maxAndBitValue = andResult;
            }
        }
    }
    // console.log(array);
    return maxAndBitValue;
}

function getMaxLessThanK(n, k) {
    return ((k | (k - 1)) > n) ? (k - 2) : (k - 1);
}

function getDayName(dateString) {
    let dayName;
    // Write your code here
    switch (new Date(dateString).getDay()) {
        case 0:
            dayName = 'Sunday';
            break;
        case 1:
            dayName = 'Monday';
            break;
        case 2:
            dayName = 'Tuesday';
            break;
        case 3:
            dayName = 'Wednesday';
            break;
        case 4:
            dayName = 'Thursday';
            break;
        case 5:
            dayName = 'Friday';
            break;
        case 6:
            dayName = 'Saturday';
            break;
    }
    return dayName;
}

testingFunction = getMaxLessThanK;

describe('Bitwise operation', function () {

    const tests = [{
        variables: [5, 2],
        expected: 1
    }, {
        variables: [8, 5],
        expected: 4
    }, {
        variables: [2, 2],
        expected: 0
    }
    ];

    tests.forEach(test => {
        it('Variables: ' + test.variables, function () {
            assert.equal(testingFunction(...test.variables), test.expected);
        });
    });
});

