// let lol = { '0': 0, '1': 0, "counter": 0 };
let lol = { "counter": 0 };

function numberOfShifts(n, compiledResults) {
    compiledResults.counter++;
    // console.log(n, numberOfShifts);

    if (n <= 1) {
        compiledResults[n] = 0;
        compiledResults[n - 1] = 0;
    }
    if (compiledResults[n] !== undefined) {
        return compiledResults[n];
    }
    else if (!compiledResults[n - 1]) {
        compiledResults[n - 1] = numberOfShifts(n - 1, compiledResults);
    }

    // This element is to remind that n - 2 element is required initially
    // if (!compiledResults[n - 2]) {
    //     compiledResults[n - 2] = numberOfShifts(n - 2, compiledResults)
    // }

    return compiledResults[n - 1] * 2 - compiledResults[n - 2] + n - 1;
}

// console.log(numberOfShifts(11, lol));
// console.log(lol);
// lol[10] = numberOfShifts(10, lol);
// console.log(`Loop has been iterated ${lol.counter} times.`);
// lol.counter = 0;
// console.log(numberOfShifts(15, lol));
// console.log(`Loop has been iterated ${lol.counter} times.`);
console.log(numberOfShifts(2, lol));
console.log(lol);
