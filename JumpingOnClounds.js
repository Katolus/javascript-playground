var assert = require('assert');

function jumpingOnClouds(c) {
    const cloudLength = c.length - 1;
    let jumpCounter = 0;
    for (let i = 0; i < cloudLength;) {
        if (c[i + 2] === 0 && i + 2 <= cloudLength) {
            i = i + 2;
        } else if (c[i + 1] === 0) {
            i++;
        } else {
            throw 'Nowhere to jump!';
        }
        jumpCounter++;
    }
    return jumpCounter;
}

jumpingOnClouds([0, 0, 1, 0, 0, 1, 0]);

let testingFunction = jumpingOnClouds;
describe('Testing function', function () {
    var tests = [{
            testVar: [0, 0, 1, 0, 0, 1, 0],
            expected: 4
        }, {
            testVar: [0, 0, 1, 0, 1, 0],
            expected: 3
        },
        {
            testVar: [],
            expected: 0
        },
        {
            testVar: [0],
            expected: 0
        }, {
            testVar: [0, 0],
            expected: 1
        }
    ];

    tests.forEach(test => {
        it('Array of Clouds: ' + test.testVar, function () {
            assert.equal(testingFunction(test.testVar), test.expected);
        });
    });
});