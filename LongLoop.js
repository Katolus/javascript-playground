function loopOverSize(Dx, Dy) {
    console.time("start");
    let counter = 0;
    // r, g ,b ,a
    function Pixel(r, g, b, a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
        return this;
    }
    console.log(new Pixel(0, 0, 0, 0));
    let setOfPixels = {};
    for (let j = 0; j < 4; j++) {
        for (let i = 0; i < Dx * Dy; i++) {
            let pixel = new Pixel(0, 0, 0, 0);
            setOfPixels[i] = pixel;
            counter++;
        }
    }


    console.log(`Number of iterations ${counter}`);
    console.timeEnd("start");
}

loopOverSize(4160, 3120);