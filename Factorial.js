function factorial(n) {
    let factorial = 1;
    while (n > 0) {
        factorial = factorial * n;
        // console.log(n);
        n -= 1;
    }
    return factorial;
}

function recFactorial(n) {
    if (n === 0) {
        return 1;
    } else {
        return n * recFactorial(n - 1);
    }
}

console.log(factorial(4));
console.log(recFactorial(4));