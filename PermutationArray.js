var assert = require('assert');

function twoArrays(k, A, B) {
    A = A.sort((a, b) => a - b);
    B = B.sort((a, b) => b - a);
    // console.log(A, B);
    const arraysLength = A.length;
    for (let i = 0; i < arraysLength; i++) {
        if (A[i] + B[i] < k) {
            return 'NO';
        }
    }

    return 'YES';
}

testingFunction = twoArrays;

describe('Permutation array', function () {

    const tests = [{
        variables: [10, [2, 1, 3], [7, 8, 9]],
        expected: 'YES'
    }, {
        variables: [5, [1, 2, 2, 1], [3, 3, 3, 4]],
        expected: 'NO'
    }
    ];

    tests.forEach(test => {
        it('Variables: ' + test.variables, function () {
            assert.equal(testingFunction(...test.variables), test.expected);
        });
    });
});

