function alternatingCharacters(s) {
    return s.map((item, index, orray) => {
            return orray[index] ?
                orray[index].length - item.split('')
                .filter((letter, index, oarray) => {
                    if (index === 0) {
                        return true;
                    } else if (letter === oarray[index - 1]) {
                        return false;
                    } else {
                        return true;
                    }
                }).length :
                undefined;
        })
        .filter(val => val !== undefined ? true : false);
}

const val = ['AAAA', 'BBBBB', 'ABABABAB', 'BABABA', 'AAABBB'];
console.log(alternatingCharacters(val));