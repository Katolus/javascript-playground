'use strict';
var assert = require("assert");


// Capitalize 0 or more lowercase letters
// Delete all the remaining lCase of a character


// Q: Given two strings, and , determine if it's possible to make equal to as described. If so, print YES on a new line. Otherwise, print NO.
const aAlphabet = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase();
function traverseBack(a, aR) {
    let aString = [];
    let firstUpperL = a.split("").findIndex((element) => aAlphabet.includes(element));
    if (firstUpperL > -1) {
        const subArray = a.slice(firstUpperL).concat(aR);
        console.log(`Looking for an subarray of ${subArray}`);
        let isThereASubArray = a.toLowerCase().indexOf(subArray.toLowerCase());
        if (isThereASubArray > -1) {
            // Bad practice to change input !! 
            console.log(`a.slice(0, firstUpperL).concat(subArray) => ${a.slice(0, firstUpperL).concat(subArray)}`);
            a = a.slice(0, firstUpperL).concat(subArray);
            return a.length - 1;
        } else {
            return -1;
        }
    } else {
        for (let i = a.length - 1; i >= 0; i--) {
            console.log(a[i], aR);
            // If it is an upper case letter cannot traverse back more
            if (a[i].toUpperCase() === a[i]) {
                // console.log(`Cannot traverse past an upper case ${a[i]}`);
                aString.concat(a.slice(i, a.length));
                return -1;
            }

            // This will never be an upper case
            if (a[i] === aR.toLowerCase()) {
                return i;
            }
        }
        return -1;
    }

}

function compareRecursively(a, b, aR) {
    console.log("Compare: ", a, b);
    if (b.length === 0) {
        return [-1, 0];
    }
    // Check if slice of B is in a
    let aRange = [a.toUpperCase().indexOf(b), b.length];
    if (aRange[0] > -1) {
        // If so check if can be waked back with replacing any upper case letters
        if (a[aRange[1] - 1].toUpperCase() === aR) {
            return [aRange[0], b];
        } else {

        }


        return [aRange[0], b];
    } else {
        compareRecursively(a, b.slice(0, b.length - 1), aR);
    }
}
function abbreviationDP(a, b) {
    // To avoid begging case
    let aNew = '';
    a = ['', ...a.split(""), ''];
    try {
        for (let i = 0, j = 0; i < a.length - 1; i++) {

            if (a[i] === '') {
                continue;
            }

            if (a[i] === a[i].toUpperCase()) {
                // Upper case
                // console.log(b.indexOf(a[i - 1] + a[i]));
                // console.log((a[i - 1] + a[i]), b);
                if (a[i] === b[j]) {
                    // If you not found a match before try
                    a[i] = a[i - 1] + a[i];
                    j++;
                } else {
                    a[i - 1].lastIndexOf(a[i]);
                    let replaceIndex = traverseBack(a[i - 1], a[i]);
                    // let canUPBeReplaced = compareRecursively(a[i - 1], b.slice(0, j), a[i]);
                    // let isBinA = a[i - 1].toUpperCase().indexOf(b.slice(0, j));
                    if (replaceIndex > -1) {
                        // If the whole previous b matches match a[i - 1], 
                        let lastV = a[i - 1].slice(0, replaceIndex).split("");
                        // if (lastV[j - 1] === lastV[j - 1].toUpperCase()) {
                        //     throw `Cannot replace an upper case letter ${lastV[j - 1]}`;
                        // }
                        lastV[replaceIndex] = a[i];
                        a[i] = lastV.join("");
                        j = replaceIndex;
                        j++;
                    } else {
                        throw "Unable to fit a capital letter in a subarray."
                    }
                    // console.log(a[i - 1], b.slice(0, j));
                }
            }

            if (a[i].toLowerCase() === a[i]) {
                // Lower case
                if (a[i].toUpperCase() === b[j]) {
                    // console.log(a[i]);
                    a[i] = a[i - 1] + a[i];
                    j++;
                } else {
                    a[i] = a[i - 1];
                }
            }
            // console.log('%s| %i', b[j], j);
            console.log(a);
        }
    } catch (e) {
        console.log(e);
        return "NO";
    }

    console.log(a[a.length - 2].toUpperCase() + " = " + b);
    if ((a[a.length - 2].toUpperCase() === b)) {
        return "YES";
    } else {
        return "NO";
    }
}
// abbreviationDP('ababbaAbAB', 'AABABB');
let testFunction = abbreviationDP;

describe("Abbreviation:", function () {

    const tests = [{
            variables: ['aAbAb', 'ABAB'],
            expected: "YES"
        }, {
            variables: ['baaBa', 'BAAA'],
            expected: "NO"
        }, {
        // Sure this is good
        variables: ['ababbaAbAB', 'AABABB'],
        expected: "NO"
        }, {
            variables: ['abAAb', 'AAA'],
            expected: "YES"
        }, {
            variables: ['babaABbbAb', 'ABAA'],
            expected: "NO"
    }];

    tests.forEach(test => {
        it("Test variables: " + test.variables, function () {
            assert.equal(testFunction(...test.variables), test.expected);
        })
    });
});