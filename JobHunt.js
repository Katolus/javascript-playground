const applications = {
    "Leanne Hewes": ["NSW Government"],
    "Darren Brown": ["Local Measure", "DiUS", "Trade Ledger"],
    "Jordan August": ["Mathspace", "Lodge Technology", "AirTasker"],
    "Zsanett Kovacs": [{ name: "Quantas", rate: "600/day" }, { name: "HealthShare", stage: "final-interview", rate: "90-140k" }],
    "Piotr Katolik": [{ name: "Safety Culture", stage: "test", contact: "Cloë Stanbridge" },
    { name: "Identitii", stage: "initial-conversation", contact: "Layla Bates" },
        "GoScript", "Code Barrel", "Accordant", "Quilr", "Atlassian", "Canva", "Woods Bagot",
        "PerSure", "Health Engine", "Domain", "Nod", "Kounta", "HealthMatch", "Genbook",
        "SolveXia", "iCIMS", "Alfie", "Hireup", "The Eclair Group"]
}