var assert = require('assert');

var firstCharacter = function(str) {
    return str.slice(0, 1);
};

// Returns the last character of a string str
var lastCharacter = function(str) {
    return str.slice(-1);
};

// Returns the string that results from removing the first
//  and last characters from str
var middleCharacters = function(str) {
    return str.slice(1, -1);
};

var isPalindrome = function(str) {
    if(str.length <= 1){
        return true;
    }else if(firstCharacter(str) !== lastCharacter(str)){
        return false;
    }else{
        return isPalindrome(middleCharacters(str));
    }
    // base case #1
    // base case #2
    // recursive case
};

var checkPalindrome = function(str) {
    console.log("Is this word a palindrome? " + str);
    console.log(isPalindrome(str));
};

// checkPalindrome("a");
assert.equal(isPalindrome("a"), true);
// checkPalindrome("motor");
assert.equal(isPalindrome("motor"), false);
// checkPalindrome("rotor");
assert.equal(isPalindrome("rotor"), true);
assert.equal(isPalindrome("ro tor"), false);
checkPalindrome("rotor");
assert.equal(isPalindrome("ro1 t 1or"), true);

