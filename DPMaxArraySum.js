'use strict';
var assert = require("assert");

function maxSubsetSum(arr) {
    // Assign 0 at end at beginning to avoid special cases
    // arr = [0, ...arr, 0];
    // let sum = 0;
    // let localSum;
    // let prevLocalSum = Number.MIN_SAFE_INTEGER;
    // for (let i = 1; i < arr.length;) {
    //     localSum = arr.getAdjacentSum(i);

    //     if (i % 2 === 0) {

    //     }

    //     prevLocalSum = localSum;
    // }

    arr[0] = Max(0, arr[0]);
    arr[1] = Max(arr[0], arr[1]);
    for (let i = 2; i < arr.length; i++) {
        // console.log(arr);
        arr[i] = Max(arr[i - 1], arr[i - 2] + arr[i]);
    }

    return arr[arr.length - 1];
}

Array.prototype.getAdjacentSum = function (i) {
    return this[i - 1] + this[i + 1];
}

function Max(a, b) {
    console.log(`${a} > ${b}`);
    return a > b ? a : b;
}

maxSubsetSum([3, 7, 4, 6, 5]);
let testingFunction = maxSubsetSum;

describe("Dynamic programming Max Array Sum:", function () {

    const tests = [{
        variables: [3, 7, 4, 6, 5],
        expected: 13
    }, {
        variables: [2, 1, 5, 8, 4],
        expected: 11
    }, {
        variables: [3, 5, -7, 8, 10],
        expected: 15

    }];

    tests.forEach(test => {
        it("Test variables: " + test.variables, function () {
            assert.equal(testingFunction(test.variables), test.expected);
        })
    })
});
