'use strict';
var assert = require('assert');

function sherlockAndAnagrams(s) {
    let counter = 0;
    let aS = s.split("");
    let dictionary = {};
    let currentWord;

    for (let j = 0; j < aS.length; j++) {

        for (let i = j + 1; i <= aS.length; i++) {
            currentWord = aS.slice(j, i).sort((a, b) => a.localeCompare(b)).join("");
            // console.log(currentWord);

            if (dictionary[currentWord]) {
                // counter++;
                counter += dictionary[currentWord];
                // console.log(`Current count = ${counter}`);
                dictionary[currentWord]++
            } else {
                dictionary[currentWord] = 1;
            }
        }
    }
    console.log(dictionary);
    console.log(`Counter ${counter}`);
    let supCounter = 0;
    function stepSum(n) {
        if (n === 0) {
            return 0;
        }
        if (n === 1) {
            return 1;
        } else {
            return n + stepSum(n - 1);
        }
    }
    for (let item in dictionary) {
        // console.log(stepSum(dictionary[item]));
        supCounter += stepSum(dictionary[item] - 1);
    }

    console.log(supCounter);

    return counter;

}


let testing = sherlockAndAnagrams;

describe('Anagram tests', function () {
    this.timeout(5000);
    const tests = [
        {
            variables: 'abba',
            expected: 4
        }, {
            variables: 'abcd',
            expected: 0
        }, {
            variables: 'ifailuhkqq',
            expected: 3
        }, {
            variables: 'abbaba',
            expected: 17
        }, {
            variables: 'aabb',
            expected: 2
        }, {
            variables: 'aa',
            expected: 1
        }, {
            variables: 'aaa',
            expected: 4
        }, {
            variables: 'aaaa',
            expected: 10
        }, {
            variables: 'aaaaa',
            expected: 20
        }, {
            variables: 'aaaaabbb',
            expected: 24
        }, {
            variables: 'aaaaabbbaa',
            expected: 57
            // }, {
            //     variables: 'aaaaaa',
            //     expected: 35
            // }, {
            //     variables: 'aaaaaaa',
            //     expected: 56
            // }, {
            //     variables: 'aaaaaaaa',
            //     expected: 84
            // }, {
            //     variables: 'aaaaaaaaa',
            //     expected: (84 * 2 - 56 + 'aaaaaaaaa'.length - 1)
            // }, {
            //     variables: 'aaaaaaaaaa',
            //     expected: (120 * 2 - 84 + 'aaaaaaaaaa'.length - 1)
            // }, {
            //     variables: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            //     expected: 166650
            // }, {
            //     variables: 'babacaccaaabaaaaaaaccaaaccaaccabcbbbabccbbabababccaabcccacccaaabaccbccccbaacbcaacbcaaaaaaabacbcbbbcc',
            //     expected: 8640
            // }, {
            //     variables: 'bbcbacaabacacaaacbbcaabccacbaaaabbcaaaaaaaccaccabcacabbbbabbbbacaaccbabbccccaacccccabcabaacaabbcbaca',
            //     expected: 9644
            // }, {
            //     variables: 'ifailuhkqqhucpoltgtyovarjsnrbfpvmupwjjjfiwwhrlkpekxxnebfrwibylcvkfealgonjkzwlyfhhkefuvgndgdnbelgruel',
            //     expected: 399
            // }, {
            //     variables: 'gffryqktmwocejbxfidpjfgrrkpowoxwggxaknmltjcpazgtnakcfcogzatyskqjyorcftwxjrtgayvllutrjxpbzggjxbmxpnde',
            //     expected: 471
            // }, {
            //     variables: 'mqmtjwxaaaxklheghvqcyhaaegtlyntxmoluqlzvuzgkwhkkfpwarkckansgabfclzgnumdrojexnrdunivxqjzfbzsodycnsnmw',
            //     expected: 370
            // }, {
            //     variables: 'ofeqjnqnxwidhbuxxhfwargwkikjqwyghpsygjxyrarcoacwnhxyqlrviikfuiuotifznqmzpjrxycnqktkryutpqvbgbgthfges',
            //     expected: 403
            // }, {
            //     variables: 'zjekimenscyiamnwlpxytkndjsygifmqlqibxxqlauxamfviftquntvkwppxrzuncyenacfivtigvfsadtlytzymuwvpntngkyhw',
            //     expected: 428
        }
    ];

    tests.forEach(test => {
        it('Test: ' + test.variables, function () {

            assert.equal(testing(test.variables), test.expected);
        })
    })
});