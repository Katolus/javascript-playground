var assert = require('assert');

function regexVar(testString) {
    /*
     * Declare a RegExp object variable named 're'
     * It must match a string that starts and ends with the same vowel (i.e., {a, e, i, o, u})
     */
    
    //  const re = new RegExp('(a).*\1');
     const re = /^([aeiou]).*\1$/;
    
    /*
     * Do not remove the return statement
     */
    return re.test(testString);
}

testingFunction = regexVar;

describe('Bitwise operation', function () {

    const tests = [{
        variables: 'bcd',
        expected: false
    }, {
        variables: 'abcd',
        expected: false
    }, {
        variables: 'abcda',
        expected: true
    }, {
        variables: 'labcda',
        expected: false
    }, {
        variables: 'aewxyzae',
        expected: false
    }
    ];

    tests.forEach(test => {
        it('Variables: ' + test.variables, function () {
            assert.equal(testingFunction(test.variables), test.expected);
        });
    });
});

