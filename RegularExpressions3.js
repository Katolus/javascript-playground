var assert = require('assert');

function regexVar(testString) {

    const re = /\d+/g;
     
    return testString.match(re);
}

testingFunction = regexVar;

describe('Bitwise operation', function () {

    const tests = [{
        variables: '1 2 3',
        expected: ['1', '2', '3']
    }
    ];

    tests.forEach(test => {
        it('Variables: ' + test.variables, function () {
            assert.deepEqual(testingFunction(test.variables), test.expected);
        });
    });
});

