const getWeekBeforeTodayDate = (howManyDaysAgo) => {
    if (typeof parseInt(howManyDaysAgo) !== 'number') {
        throw "Illegal number of days";
    } else if (howManyDaysAgo > 31 || howManyDaysAgo < 1) {
        throw "Value needs to be a number";
    } else {
        let weekBefore;
        const isLeapYear = (year) => {
            let leap = false;

            if (year % 4 === 0) {
                leap = true;
            }

            if (year % 100 === 0) {
                leap = false;
            }

            if (year % 400 === 0) {
                leap = true;
            }
            return leap;
        };
        const todayArray = new Date().toISOString().slice(0, 10).split("-");
        let currentYear = parseInt(todayArray[0]);
        let currentMonth = parseInt(todayArray[1]);
        let currentDay = parseInt(todayArray[2]);

        const tempDate = new Date(currentYear, currentMonth - 1, currentDay - howManyDaysAgo + 1).toISOString().slice(0, 10);


        const monthLength = [31, isLeapYear() ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        if (currentDay - howManyDaysAgo <= 0) {
            currentDay = monthLength[currentMonth] - (- (currentDay - howManyDaysAgo));
            if (currentMonth - 1 <= 0) {
                weekBefore = new Date(currentYear - 1, 12, currentDay).toISOString().slice(0, 10);
            } else {
                weekBefore = new Date(currentYear, currentMonth - 1, currentDay).toISOString().slice(0, 10);
            }
        } else {
            weekBefore = new Date(currentYear, currentMonth, currentDay - howManyDaysAgo).toISOString().slice(0, 10);
        }
        return weekBefore;
    }
}

console.log(getWeekBeforeTodayDate(7));