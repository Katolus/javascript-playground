function checkMagazine(magazine, note) {
    console.time("es6");
    console.log(magazine, note);
    const isGood = note.reduce((acc, nWord) => {
        if (!acc) {
            return acc;
        } else {
            magazine.reduce((acc, mWord) => {
                console.log(acc, mWord, nWord);
                acc ?  acc: () =>  nWord === mWord;
            }, false);
        }
    }, true);
    console.timeEnd("es6");
    return isGood;
}

function easyCheckMagazine(magazine, note) {
    console.time("old");
    let isGood = true;
    for (let nWord of note) {
        if (magazine.includes(nWord)) {
            // delete magazine[magazine.indexOf(nWord)];
            magazine.splice(magazine.indexOf(nWord), 1);
            // console.log(magazine);
        } else {
            isGood = false;
            break;
        }
    }
    console.timeEnd("old");
    return isGood;
}
console.time("start");
console.log(
    checkMagazine("two times three is not four".split(' '), "two times two is four".split(' ')) ?
    "Yes" :
    "No"
);
console.log(
    easyCheckMagazine("two times three is not four".split(' '), "two times two is four".split(' ')) ?
    "Yes" :
    "No"
);
console.log(
    easyCheckMagazine("give me one grand today night".split(' '), "give one grand today".split(' ')) ?
    "Yes" :
    "No"
);
console.log(
    checkMagazine("give me one grand today night".split(' '), "give one grand today".split(' ')) ?
    "Yes" :
    "No"
);
console.log(
    easyCheckMagazine("carefully check check hope in properly sentence the this words you".split(' '), "check check properly this words".split(' ')) ?
    "Yes" :
    "No"
);
console.timeEnd("start");