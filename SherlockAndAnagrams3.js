'use strict';
var assert = require('assert');

function sherlockAndAnagrams(s) {
    let counter = 0;
    let aS = s.split("");
    let letterSet = {};

    for (let letter of aS) {
        letterSet[letter] !== undefined ? letterSet[letter]++ : letterSet[letter] = 1;
    }
    console.log('Letter set:');
    console.log(letterSet);

    let memoizationObject = { "counter": 0 };

    for (let letter in letterSet) {
        // counter += numberOfShifts(letterSet[letter], memoizationObject);
        counter += numberOfShifts(letterSet[letter], memoizationObject);
    }

    console.log(memoizationObject);

    return counter;

}

function numberOfShifts(n, compiledResults) {
    compiledResults.counter++;

    if (n <= 1) {
        compiledResults[n] = 0;
        compiledResults[n - 1] = 0;
    }
    if (compiledResults[n] !== undefined) {
        return compiledResults[n];
    }
    else if (!compiledResults[n - 1]) {
        compiledResults[n - 1] = numberOfShifts(n - 1, compiledResults);
    }

    return compiledResults[n - 1] * 2 - compiledResults[n - 2] + n - 1;
}

let testing = sherlockAndAnagrams;

describe('Anagram tests', function () {
    this.timeout(5000);
    const tests = [
        {
            variables: 'abba',
            expected: 4
        }, {
            variables: 'abcd',
            expected: 0
        }, {
            variables: 'aa',
            expected: 1
        }, {
            variables: 'abbaba',
            expected: 17
            // }, {
            //     variables: 'aaa',
            //     expected: 4
            // }, {
            //     variables: 'aaaa',
            //     expected: 10
            // }, {
            //     variables: 'aaaaa',
            //     expected: 20
            // }, {
            //     variables: 'aaaaabbb',
            //     expected: 24
            // }, {
            //     variables: 'aaaaabbbcc',
            //     expected: 25
            // }, {
            //     variables: 'aaaaaa',
            //     expected: 35
            // }, {
            //     variables: 'aaaaaaa',
            //     expected: 56
            // }, {
            //     variables: 'aaaaaaaa',
            //     expected: 84
            // }, {
            //     variables: 'aaaaaaaaa',
            //     expected: (84 * 2 - 56 + 'aaaaaaaaa'.length - 1)
            // }, {
            //     variables: 'aaaaaaaaaa',
            //     expected: (120 * 2 - 84 + 'aaaaaaaaaa'.length - 1)
            //     variables: 'ifailuhkqqhucpoltgtyovarjsnrbfpvmupwjjjfiwwhrlkpekxxnebfrwibylcvkfealgonjkzwlyfhhkefuvgndgdnbelgruel',
            //     expected: 399
            // }, {
            //     variables: 'gffryqktmwocejbxfidpjfgrrkpowoxwggxaknmltjcpazgtnakcfcogzatyskqjyorcftwxjrtgayvllutrjxpbzggjxbmxpnde',
            //     expected: 471
            // }, {
            //     variables: 'mqmtjwxaaaxklheghvqcyhaaegtlyntxmoluqlzvuzgkwhkkfpwarkckansgabfclzgnumdrojexnrdunivxqjzfbzsodycnsnmw',
            //     expected: 370
            // }, {
            //     variables: 'ofeqjnqnxwidhbuxxhfwargwkikjqwyghpsygjxyrarcoacwnhxyqlrviikfuiuotifznqmzpjrxycnqktkryutpqvbgbgthfges',
            //     expected: 403
            // }, {
            //     variables: 'zjekimenscyiamnwlpxytkndjsygifmqlqibxxqlauxamfviftquntvkwppxrzuncyenacfivtigvfsadtlytzymuwvpntngkyhw',
            //     expected: 428
        }
    ];

    tests.forEach(test => {
        it('Test: ' + test.variables, function () {

            assert.equal(testing(test.variables), test.expected);
        })
    })
});