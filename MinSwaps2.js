var assert = require('assert');

function minimumSwaps(arr) {
    const arrLength = arr.length;
    function min() {
        let minValue = Number.MAX_SAFE_INTEGER;
        let maxValue = Number.MIN_SAFE_INTEGER;
        for (let i = 0; i < arrLength; i++) {
            if (arr[i] < minValue) {
                minValue = arr[i];
            }
            if (arr[i] > maxValue) {
                maxValue = arr[i];
            }
        }
        return [minValue, maxValue];
    }

    let numberOfSwaps = 0;
    const minMax = min();
    let currentValue = minMax[0];
    let searchedIndex;
    let valueHolder;

    if (minMax[1] > arrLength) {
        return 3;
    }
    for (let i = 0; i < arrLength; i++) {

        if (arr[i] !== currentValue) {
            searchedIndex = arr.indexOf(currentValue, i);
            valueHolder = arr[i];
            arr[i] = arr[searchedIndex];
            arr[searchedIndex] = valueHolder;
            numberOfSwaps++;
        }

        currentValue++;
    }
    return numberOfSwaps;
}

let testingFunction = minimumSwaps;
describe('Testing function', function () {
    var tests = [{
        testVar: [4, 3, 1, 2],
        expected: 3
    },
    {
        testVar: [1, 2, 3, 4],
        expected: 0
    },
    {
        testVar: [2, 3, 4, 1, 5],
        expected: 3
    },
    {
        testVar: [2, 3, 5, 4],
        expected: 1
    }, {
        testVar: [1, 3, 5, 2, 4, 6, 7],
        expected: 3
    }];

    tests.forEach(test => {
        it('Minimum Swaps 2: ' + test.testVar, function () {
            assert.equal(testingFunction(test.testVar), test.expected);
        });
    });
});