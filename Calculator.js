const calculator = function () {
    let total = 0;
    return {
        add: function (number) {
            total = total + number;
        },
        sub: function (number) {
            total = total - number;
        },
        divide: function (number) {
            total = total / number;
        },
        multiply: function (number) {
            total = total * number;
        },
        total: function () {
            return total;
        }
    };
}

module.exports = calculator();