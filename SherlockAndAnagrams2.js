'use strict';
var assert = require('assert');

function sherlockAndAnagrams(s) {
    let counter = 0;
    let aS = s.split("");
    let subWords = {};
    let innerCounter, innerArray, currentWord, range;


    for (let j = aS.length; j >= 1; j--) {

        range = [0, j];
        subWords[range[1] - range[0]] = [];
        while (range[1] < aS.length + 1) {
            currentWord = aS.slice(range[0], range[1]);
            innerArray = subWords[range[1] - range[0]];

            // console.log(currentWord);
            innerCounter = innerArray.filter(arrayOfLetters => compareValuesInTwoArrays(arrayOfLetters, currentWord)).length;

            subWords[range[1] - range[0]].push(aS.slice(range[0], range[1]));
            // subWords.push(aS.slice(range[0], range[1]));
            counter += innerCounter;
            range[0]++;
            range[1]++;
        }
        // console.log(subWords);
    }

    // console.log('Creating words finished');
    return counter;

}

Array.prototype.myIndexOf = function (char, arrayOfExcludedIndexes) {
    for (let i = 0; i < this.length; i++) {
        if (arrayOfExcludedIndexes.indexOf(i) > -1) {
            continue;
        }
        if (this[i] === char) {
            return i;
        }
    }
    return -1;
}


function compareValuesInTwoArrays(a1, a2) {
    if (a1.length !== a2.length) {
        return false;
    }

    let foundIndexes = [];
    let currentIndex;
    for (let i = 0; i < a1.length; i++) {

        currentIndex = a2.myIndexOf(a1[i], foundIndexes);

        if (currentIndex > -1) {
            foundIndexes.push(currentIndex);
        } else {
            return false;
        }

    }
    console.log(`Array with same set of values: ${a1} and ${a2}`);
    return true;
}

let testing = sherlockAndAnagrams;

describe('Anagram tests', function () {
    this.timeout(5000);
    const tests = [
        {
            variables: 'abba',
            expected: 4
        }, {
            variables: 'abcd',
            expected: 0
        }, {
            variables: 'ifailuhkqq',
            expected: 3
        }, {
            variables: 'abbaba',
            expected: 3
        // }, {
        //     variables: 'aabb',
        //     expected: 2
        // }, {
        //     variables: 'aa',
        //     expected: 1
        // }, {
        //     variables: 'aaa',
        //     expected: 4
        // }, {
        //     variables: 'aaaa',
        //     expected: 10
        // }, {
        //     variables: 'aaaaa',
        //     expected: 20
        // }, {
        //     variables: 'aaaaabbb',
        //     expected: 24
        // }, {
        //     variables: 'aaaaabbbaa',
        //     expected: 57
        // }, {
        //     variables: 'aaaaaa',
        //     expected: 35
        // }, {
        //     variables: 'aaaaaaa',
        //     expected: 56
        // }, {
        //     variables: 'aaaaaaaa',
        //     expected: 84
        // }, {
        //     variables: 'aaaaaaaaa',
        //     expected: (84 * 2 - 56 + 'aaaaaaaaa'.length - 1)
        // }, {
        //     variables: 'aaaaaaaaaa',
        //     expected: (120 * 2 - 84 + 'aaaaaaaaaa'.length - 1)
        // }, {
        //     variables: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        //     expected: 166650
        // }, {
        //     variables: 'babacaccaaabaaaaaaaccaaaccaaccabcbbbabccbbabababccaabcccacccaaabaccbccccbaacbcaacbcaaaaaaabacbcbbbcc',
        //     expected: 8640
        // }, {
        //     variables: 'bbcbacaabacacaaacbbcaabccacbaaaabbcaaaaaaaccaccabcacabbbbabbbbacaaccbabbccccaacccccabcabaacaabbcbaca',
        //     expected: 9644
            //     variables: 'ifailuhkqqhucpoltgtyovarjsnrbfpvmupwjjjfiwwhrlkpekxxnebfrwibylcvkfealgonjkzwlyfhhkefuvgndgdnbelgruel',
            //     expected: 399
            // }, {
            //     variables: 'gffryqktmwocejbxfidpjfgrrkpowoxwggxaknmltjcpazgtnakcfcogzatyskqjyorcftwxjrtgayvllutrjxpbzggjxbmxpnde',
            //     expected: 471
            // }, {
            //     variables: 'mqmtjwxaaaxklheghvqcyhaaegtlyntxmoluqlzvuzgkwhkkfpwarkckansgabfclzgnumdrojexnrdunivxqjzfbzsodycnsnmw',
            //     expected: 370
            // }, {
            //     variables: 'ofeqjnqnxwidhbuxxhfwargwkikjqwyghpsygjxyrarcoacwnhxyqlrviikfuiuotifznqmzpjrxycnqktkryutpqvbgbgthfges',
            //     expected: 403
            // }, {
            //     variables: 'zjekimenscyiamnwlpxytkndjsygifmqlqibxxqlauxamfviftquntvkwppxrzuncyenacfivtigvfsadtlytzymuwvpntngkyhw',
            //     expected: 428
        }
    ];

    tests.forEach(test => {
        it('Test: ' + test.variables, function () {

            assert.equal(testing(test.variables), test.expected);
        })
    })
});