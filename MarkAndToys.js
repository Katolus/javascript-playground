var assert = require('assert');

function maximumToys(prices, k) {
    console.log(prices);
    prices.sort((a, b) => a - b);
    console.log(prices);
    let sum = 0;
    let toys = [];
    for (let price of prices) {
        sum += price;
        toys.push(price);
        console.log(sum, toys);
        if (sum > k) {
            break;
        }
    }
    return toys.length - 1;

}

let testingFunction = maximumToys;

describe('Mark and Toy tests', function() {

    const tests = [
        {
            variables: [[1, 12, 5, 111, 200, 1000, 10], 50],
            expected: 4
        }, {
            variables: [[1, 2, 3, 4], 7],
            expected: 3
        }, {
            variables: [[3, 7, 2, 9, 4], 15],
            expected: 3
        }
    ];

    tests.forEach(test => {
        it('Test:' + test.variables, function() {
            assert.equal(testingFunction(...test.variables), test.expected);
        });
    });
});
