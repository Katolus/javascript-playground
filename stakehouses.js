function nearestXsteakHouses(totalSteakhouses, allLocations, numSteakhouses) {
    // WRITE YOUR CODE HERE
    var computationArray = allLocations.map(array => {
        return array.reduce((acc, item) => acc += item * item);
    }).map((item, index) => {
        return [item, index];
    }).sort((a, b) => a[0] - b[0]).slice(0, numSteakhouses);

    var returnArray = [];
    for (var item of computationArray) {
        returnArray.push(allLocations[item[1]]);
    }
    return returnArray;

}

function removeObstacle(numRows, numColumns, lot) {
    // WRITE YOUR CODE HERE
    if (1 >= numRows || numColumns >= 1000) {
        console.log(`Value out of range`);
        return -1;
    } else {
        var x = 0;
        var y = 0;
        var moveCounter = 0;
        if (lot[x][y] === 0) {
            console.log('Robot cannot enter on trenches!');
            return -1;
        }
        function drawLot(x, y, char) {
            lot[x][y] = char;
            lot.map(array => console.log(array));
        }
        drawLot(x, y, '*');
        function localizeObstacle() {
            let indexX, indexY;
            lot.map((array, index) => {
                array.filter((val, index) => {
                    if (val === 9) {
                        indexX = index;
                        return true;
                    }
                }) !== undefined ? indexY = index : null;
            })
            return [indexX, indexY];
        }

        function okToMove(x, y) {
            return lot[x][y] === 1 ? true : false;
        }
        const target = localizeObstacle();
        while (x !== target[0] || y !== target[1]) {
            console.log(target, x, y);

            let char;
            if (x < target[0] && okToMove(x + 1, y)) {
                char = '->';
                x = x + 1;
                console.log(`Move to x -> ${x}`);
                moveCounter++;
            } else if (x > target[0] && okToMove(x - 1, y)) {
                x = x - 1;
                char = '<-';
                console.log(`Move to x -> ${x}`);
                moveCounter++;

            } else if (y < target[1] && okToMove(x, y + 1)) {
                y = y + 1;
                console.log(`Move to y -> ${y}`);
                char = '^';
                moveCounter++;
            }
            else if (y > target[1] && okToMove(x, y - 1)) {
                y = y - 1;
                char = '|';
                console.log(`Move to y -> ${y}`);
                moveCounter++;
            } else {
                console.log(`No way to move`);
                break;
            }
            // console.log(x, y);
            drawLot(x, y, char);
        }


        return moveCounter;
    }

}

// console.log(nearestXsteakHouses(3, [[1, -3], [1, 2], [3, 4]], 1));
const lot = [[1, 1, 1, 1], [0, 1, 1, 1], [0, 1, 1, 1], [1, 1, 9, 1], [0, 0, 1, 1]];
console.log(removeObstacle(2, 991, lot));

// for(let array of allLocations){
//     array[0] * array[0] + array[1] * array[1] 
// }

// .sort((a, b) => a - b)