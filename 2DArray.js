// Complete the hourglassSum function below.
function hourglassSum(arr) {
    let globalArray = [];
    for (let i = 1; i < arr.length - 1; i++) {
        let localArray = [];
        for (let j = 1; j < arr[i].length - 1; j++) {
            let acc = arr[i - 1][j] + arr[i - 1][j - 1] + arr[i - 1][j + 1] + arr[i][j] + arr[i + 1][j - 1] + arr[i + 1][j] + arr[i + 1][j + 1];
            localArray[j - 1] === undefined ? localArray[j - 1] = acc : localArray[j - 1] += acc;
        }
        globalArray[i - 1] === undefined ? globalArray[i - 1] = localArray : globalArray[i - 1] += localArray;
    }

    function findTheBiggetValue() {
        return globalArray.reduce((acc, array) => {
            const localValue = array.reduce((acc, item, index, oarray) => {
                console.log(index, oarray);
                return acc > item ? acc : acc = item;
            });
            return localValue < acc ? acc : acc = localValue;
        }, globalArray[0]);
    }
    return findTheBiggetValue();
}

function main() {
    let array2 = [
        [1, 1, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 0],
        [0, 0, 2, 4, 4, 0],
        [0, 0, 0, 2, 0, 0],
        [0, 0, 1, 2, 4, 0]
    ];
    let array3 = [
        [-1, 1, -1, 0, 0, 0],
        [0, -1, 0, 0, 0, 0],
        [-1, -1, -1, 0, 0, 0],
        [0, -9, 2, -4, -4, 0],
        [-7, 0, 0, -2, 0, 0],
        [0, 0, -1, -2, -4, 0]
    ];
    console.log(hourglassSum(array3));
}

function test() {
    let array = [1, 2 ,3 ,4];
    array.reduce((acc, val, index) => {
        console.log(index);
        return acc + val;
    });
}
// test();
main();