var assert = require('assert');

function countingValleys(n, s) {
    const splitString = s.split("");
    const state = {
        currentAltitude: splitString.length > 0 ? 0 : 1,
        numberOfValleys: 0
    };


    for (let char of splitString) {

        // state.currentAltitude === 'start' ? state.currentAltitude = 0 : undefined;
        switch (char) {
            case "U":
                state.currentAltitude++;
                state.currentAltitude === 0 ? state.numberOfValleys++ : undefined;
                break;
            case "D":
                state.currentAltitude--;
                break;
        }
        // console.log(state);
    }
    console.log(splitString.map(char => {
        if (char === 'U') {
            return '/';
        } else {
            return '\\';
        }
    }).join(""));
    return state.numberOfValleys;
}

let testingFunction = countingValleys;
describe('Testing function', function () {
    var tests = [{
            testingString: 'UDDDUDUU',
            expected: 1
        },
        {
            testingString: 'UDDDUDUUD',
            expected: 1
        },
        {
            testingString: 'U',
            expected: 0
        },
        {
            testingString: 'UDUDUDUD',
            expected: 0
        },
        {
            testingString: '',
            expected: 0
        },
        {
            testingString: 'DUDUUD',
            expected: 2
        }
    ];

    tests.forEach(test => {
        it('String: ' + test.testingString, function () {
            assert.equal(testingFunction(8, test.testingString), test.expected);
        });
    });
});