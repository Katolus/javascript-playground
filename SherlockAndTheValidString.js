'use strict';
var assert = require("assert");

//Sherlock considers a string to be valid if all characters of the string appear the same number of times. 
//It is also valid if he can remove just 1 character at 1 index in the string, and the remaining characters will occur the same number of times.
function isValid(s) {

    let freq = {};
    let currentLetter;
    let maxValue = 0;
    let minValue = Number.MAX_SAFE_INTEGER;
    let secondLargest = 0;
    if (s.length === 1) {
        return "YES";
    }

    for (let i = 0; i < s.length; i++) {
        currentLetter = s[i];
        if (freq[currentLetter]) {
            freq[currentLetter]++;
        } else {
            // counter++;
            freq[currentLetter] = 1;
        }
    }

    let oneCounter = 0;
    for (let letter in freq) {
        if (maxValue < freq[letter]) {
            secondLargest = maxValue;
            maxValue = freq[letter];
        } else if (secondLargest < freq[letter]) {
            secondLargest = freq[letter];
        }
        if (minValue > freq[letter]) {
            minValue = freq[letter];
        }
        if (freq[letter] === 1) {
            oneCounter++;
        }
    }

    // let retyr; 
    console.log(freq, oneCounter);
    console.log(maxValue, secondLargest, minValue);
    switch (true) {
        case (maxValue === secondLargest && oneCounter === 1):
        case (maxValue === secondLargest && maxValue === minValue):
            return "YES";
        case (maxValue - minValue !== 1):
        case (secondLargest !== minValue && oneCounter !== 1):
            return "NO";
        default:
            return "YES";
    }

}

let testingFunction = isValid;

describe("Sherlock and the valid string:", function () {

    const tests = [{
        variables: 'aabbcd',
        expected: "NO"
    }, {
        variables: 'aabbccddeefghi',
        expected: "NO"
    }, {
        variables: 'abcdefghhgfedecba',
        expected: "YES"
    }, {
        variables: 'aaaabbcc',
        expected: "NO"
    }, {
        variables: 'abbac',
        expected: "YES"
    }, {
        variables: 'aabbcc',
        expected: "YES"
    }, {
        variables: 'ibfdgaeadiaefgbhbdghhhbgdfgeiccbiehhfcggchgghadhdhagfbahhddgghbdehidbibaeaagaeeigffcebfbaieggabcfbiiedcabfihchdfabifahcbhagccbdfifhghcadfiadeeaheeddddiecaicbgigccageicehfdhdgafaddhffadigfhhcaedcedecafeacbdacgfgfeeibgaiffdehigebhhehiaahfidibccdcdagifgaihacihadecgifihbebffebdfbchbgigeccahgihbcbcaggebaaafgfedbfgagfediddghdgbgehhhifhgcedechahidcbchebheihaadbbbiaiccededchdagfhccfdefigfibifabeiaccghcegfbcghaefifbachebaacbhbfgfddeceababbacgffbagidebeadfihaefefegbghgddbbgddeehgfbhafbccidebgehifafgbghafacgfdccgifdcbbbidfifhdaibgigebigaedeaaiadegfefbhacgddhchgcbgcaeaieiegiffchbgbebgbehbbfcebciiagacaiechdigbgbghefcahgbhfibhedaeeiffebdiabcifgccdefabccdghehfibfiifdaicfedagahhdcbhbicdgibgcedieihcichadgchgbdcdagaihebbabhibcihicadgadfcihdheefbhffiageddhgahaidfdhhdbgciiaciegchiiebfbcbhaeagccfhbfhaddagnfieihghfbaggiffbbfbecgaiiidccdceadbbdfgigibgcgchafccdchgifdeieicbaididhfcfdedbhaadedfageigfdehgcdaecaebebebfcieaecfagfdieaefdiedbcadchabhebgehiidfcgahcdhcdhgchhiiheffiifeegcfdgbdeffhgeghdfhbfbifgidcafbfcd',
        expected: "YES"
    }];

    tests.forEach(test => {
        it("Test variables: ", function () {
            assert.equal(testingFunction(test.variables), test.expected);
        })
    })
});
