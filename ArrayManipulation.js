'use strict';

var assert = require("assert");

function arrayManipulation(n, queries) {

    console.time('Start');
    let initialArray = [];
    console.time('Initialization loop');
    // Initialize array
    for (let i = 0; i < n; i++) {
        initialArray.push(0);
    }

    console.timeEnd('Initialization loop');

    let maximum = Number.MIN_SAFE_INTEGER;

    // console.log(initialArray);
    // Manipulate array
    console.time('For Each');

    queries.forEach(array => {
        for (let i = array[0] - 1; i < array[1]; i++) {
            initialArray[i] += array[2];
        }
    });

    console.timeEnd('For Each');
    // Search for maximum
    console.time('Maximum');

    for (let val of initialArray) {
        if (val > maximum) maximum = val;
    }
    console.timeEnd('Maximum');
    // console.log(initialArray);
    console.timeEnd('Start');
    return maximum;
}

function arrayManipulation2(n, queries) {

    console.time('Start');
    let initialArray = [];
    let maximum = Number.MIN_SAFE_INTEGER;
    let sum = 0;
    console.time('Initialization loop');
    // Initialize array
    for (let i = 0; i < n; i++) {
        initialArray[i] = 0;
    }

    console.timeEnd('Initialization loop');

    // Manipulate array
    console.time('For Each');

    queries.forEach(array => {
        var a = array[0];
        var b = array[1];
        var k = array[2];

        initialArray[a - 1] += k;
        if (b < n) initialArray[b] -= k;
    });

    console.timeEnd('For Each');
    // Search for maximum
    console.time('Maximum');

    for (let i = 0; i < n; i++) {
        sum += initialArray[i];
        if (maximum < sum) maximum = sum;
    }
    console.timeEnd('Maximum');
    // console.log(initialArray);
    console.timeEnd('Start');
    return maximum;
}

let testFunction = arrayManipulation;
describe("Array manipulation test cases:", function () {
    const arrayOfTests = [{
        variables: [5, [[1, 2, 100], [2, 5, 100], [3, 4, 100]]],
        expected: 200
    }]
    arrayOfTests.forEach((test, index) => {
        it(`Testing ${index} -> ${test.variables}`, function () {
            assert.equal(testFunction(...test.variables), test.expected);
        });
    });
});