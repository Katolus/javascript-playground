// Any person in the queue can bribe the person directly in front of them to swap positions
// if two people swap, positions, they still wear the same sticker denoting their original places in line
// One person can bribe at most two others
var assert = require("assert");

function minimumBribes(finalState) {
    // console.log(`Final array -> ${finalState}`);
    let isChaotic = false;
    let globalCounter = 0;

    function Person(index) {
        this.initialPosition = index;
        this.index = index - 1;
        this.numberOfSwaps = 0;
        this.moveTo = (index) => {
            if (index === this.index) {
                console.log("Can't move to the same index");
            } else if (this.numberOfSwaps < 2 && perturbationArray[index].numberOfSwaps < 2) {
                const swapedPostion = perturbationArray[index].initialPosition;
                perturbationArray[index].initialPosition = this.initialPosition;
                this.initialPosition = swapedPostion;
                perturbationArray[index].numberOfSwaps++;
                this.numberOfSwaps++;
                globalCounter++;
            } else {
                let array = [];
                // console.log(`Couldn't move this person ${index} with initial position ${perturbationArray[index].initialPosition}`);
                // console.log(`Value with index ${this.index} has ${this.numberOfSwaps} and value with index ${index} has ${perturbationArray[index].numberOfSwaps}`);
                if (this.numberOfSwaps === 2) {
                    array.push(this.index);
                }

                if (perturbationArray[index].numberOfSwaps === 2) {
                    array.push(index);
                }
                return array;
            }
        };
    }

    function evaluateCorrectLine() {
        return perturbationArray.map((person, index) => {
            return person.initialPosition === finalState[index];
        });
    }
    let perturbationArray = [];
    for (let i = 1; i <= finalState.length; i++) {
        perturbationArray.push(new Person(i));
    }

    let statusArray;
    let noMoreMoves = [];
    let returnIndex;
    while (evaluateCorrectLine().filter(item => item === false).length !== 0) {
        statusArray = evaluateCorrectLine()
            .filter(item => item === false)
            .map((item, index) => index)
            .filter(item => {
                return !noMoreMoves.includes(item);
            });
        if (statusArray.length <= 1) {
            isChaotic = true;
            break;
        }
        returnIndex = perturbationArray[statusArray.pop()].moveTo(statusArray.pop());
        returnIndex !== undefined ? noMoreMoves.push(...returnIndex) : null;
        // console.log(perturbationArray.map(person => person.initialPosition));
        // console.log(`No more moves - > ${noMoreMoves}`);
    }

    // console.log(evaluateCorrectLine());
    // console.log(evaluateCorrectLine().filter(item => item === false).length === 0);

    if (isChaotic) {
        return 'Too chaotic';
    } else {
        return globalCounter;
    }
}

function minimumBribes2(finalState) {
    let isTooChaotic = false;
    let numberOfBribes = 0;
    let que = [];
    let initialQue = [];
    let swapLimits = [];
    const queLength = finalState.length;
    // Initialize que
    for (let i = 0; i < queLength; i++) {
        que.push(i + 1);
        initialQue.push(i + 1);
        swapLimits.push(0);
    }

    // Swap function which doesn't require a close match 
    function swap(startPos, endPos) {
        let valueHolder;
        console.log(que);
        console.log(swapLimits);
        initialPosE = initialQue.indexOf(que[endPos]);
        initialPosS = initialQue.indexOf(que[startPos]);
        // Swapping uses both limits
        if (swapLimits[initialPosE] < 2 && swapLimits[initialPosS] < 2) {
            valueHolder = que[endPos];
            que[endPos] = que[startPos];
            que[startPos] = valueHolder;
            swapLimits[initialPosE]++;
            swapLimits[initialPosS]++;
            numberOfBribes++;
        } else {
            isTooChaotic = true;
        }

        // Swapping uses limit of the person performing posA
    }

    // function canMove() {
    //     return swapLimits.filter(val => val !== 2).length > 1;
    // }


    let notMatchArray = finalState.slice();
    let seek;
    let seekPosition;
    let currentPosition;
    while (notMatchArray.length !== 0 && !isTooChaotic) {

        seek = notMatchArray[0];
        seekPosition = finalState.indexOf(seek);
        currentPosition = que.indexOf(seek);

        // Walking singular steps
        while (seekPosition !== currentPosition) {
            if (seekPosition > currentPosition) {
                swap(currentPosition, currentPosition + 1);
                currentPosition++;
            } else {
                swap(currentPosition, currentPosition - 1);
                currentPosition--;
            }
        }
        // swap(seekPosition, currentPosition);

        // if (!canMove()) {
        //     isTooChaotic = true
        //     break;
        // }
        notMatchArray = finalState.filter((position, index) => que[index] !== position);
    }

    if (isTooChaotic) {
        return 'Too chaotic';
    } else {
        return numberOfBribes;
    }
}

function minimumBribes3(finalState) {
    const queLength = finalState.length;
    let matchedArray = [];
    let isTooChaotic = false;
    let numberOfBribes = 0;
    let val;
    for (let k = 0; k < queLength; k++) {
        val = finalState[k];
        matchedArray.push(val);
        if (val <= matchedArray.length) {
            continue;
        } else {
            let counter = 0;
            for (let i = k + 1; i < queLength; i++) {
                if (finalState[i] < val) {
                    counter++;
                } else {
                    continue;
                }

                if (counter > 2) {
                    isTooChaotic = true;
                    break;
                } else if (counter === val - 1 - k) {
                    break;
                }
            }
        }

        if (isTooChaotic) {
            return 'Too chaotic';
        }

        numberOfBribes = numberOfBribes + counter;
    }
    return numberOfBribes;
}

function discussionResult(q) {
    let bribeCount = []
    let high = 0;

    for (let i = 0; i < q.length; i++) {
        let val = q[i]
        bribeCount[val] = 0
        if (val < high) {
            // if current value < high value, increment value for all bribeCount indices > val
            for (let j = val + 1; j < bribeCount.length; j++) {
                // Adding to empty objects results in NaN
                bribeCount[j]++
                if (bribeCount[j] > 2) {
                    return "Too chaotic";
                }
            }
        } else {
            high = val; // update the highest value encountered
        }
        console.log(bribeCount);
    }
    return bribeCount.reduce((a, b) => a + b, 0); // sum work while empty + 1 = NaN
}
// minimumBribes3([2, 4, 1, 5, 3, 7, 6, 9, 8, 10, 13, 12, 17, 11, 14, 16, 19, 18, 22]);
// minimumBribes3([2, 5, 1, 3, 4]);

let testFunction = discussionResult;
describe("NewYearChaos test cases:", function () {
    const arrayOfTests = [{
        variables: [2, 1, 5, 3, 4],
        expected: 3
    },
    {
        variables: [2, 5, 1, 3, 4],
        expected: 'Too chaotic'
    }, {
        variables: [5, 1, 2, 3, 7, 8, 6, 4],
        expected: 'Too chaotic'
    }, {
        variables: [1, 2, 5, 3, 7, 8, 6, 4],
        expected: 7
    }, {
        variables: [2, 4, 1, 5, 3, 7, 6, 9, 8, 10, 13, 12, 17, 11, 14, 16, 19, 18, 22],
        expected: 'Too chaotic'
    }]
    arrayOfTests.forEach((test, index) => {
        it(`Testing ${index} -> ${test.variables}`, function () {
            assert.equal(testFunction(test.variables), test.expected);
        });
    });
});