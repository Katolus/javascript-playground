var assert = require("assert");

// This function partitions given array and returns 
// the index of the pivot
var partition = function (array, p, r) {
    // This code has been purposefully obfuscated,
    // as you will implement it yourself in next challenge
    var e = array, t = p, n = r; var r = function (e, t, n) { var r = e[t]; e[t] = e[n]; e[n] = r; }; var i = t; for (var s = t; s < n; s++) { if (e[s] <= e[n]) { r(e, s, i); i++; } } r(e, n, i); return i;
};


function quickSort(array, p, r) {

    if (r - p > 0){
        var pIdx = partition(array, p, r);
        console.log(pIdx);
        quickSort(array, p, pIdx - 1);
        quickSort(array, pIdx + 1, r);
    }
    return array;
}

testingFunction = quickSort;

describe('Quicksort array', function () {

    const tests = [{
        variables: [[9, 7, 5, 11, 12, 2, 14, 3, 10, 6], 0, 9],
        expected: [2, 3, 5, 6, 7, 9, 10, 11, 12, 14]
    }, {
        variables: [-1, 22, 0, 4, 6, 23, 11, -4, 5, 6, 19, 99],
        expected: [-4, -1, 0, 4, 5, 6, 6, 11, 19, 22, 23, 99]
    }
    ];

    tests.forEach(test => {
        it('Variables: ' + test.variables, function () {
            assert.deepEqual(testingFunction(...test.variables), test.expected);
        });
    });
});