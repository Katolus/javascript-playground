'use strict';
var assert = require("assert");
var fs = require('fs');
const wf = require("./WriteToFile");

// Capitalize 0 or more lowercase letters
// Delete all the remaining lCase of a character


// Q: Given two strings, and , determine if it's possible to make equal to as described. If so, print YES on a new line. Otherwise, print NO.


function abbreviation(a, b) {

    const aAlphabet = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase();
    a = a.split("");
    b = b.split("");
    let uIndex
    let lIndex;
    try {
        b.forEach((letter, index) => {
            // Upper case exist
            uIndex = a.indexOf(letter);
            if (uIndex > -1) {
                // console.log(`${letter} === ${a[uIndex]} => ${letter === a[uIndex]}`);
                a[uIndex] = 0;
                // a.unshift(a[uIndex]);
                return uIndex;
            }

            // Lower case exist
            lIndex = a.indexOf(letter.toLowerCase());
            if (lIndex > -1) {
                // console.log(`${letter} === ${a[lIndex]} => ${letter === a[lIndex]}`);
                a[lIndex] = 0;
                return lIndex;
            }

            // If not break and return NOOOOOOOOOOOO!
            throw `No match for ${letter}!`;

        });

        a = a.filter(letter => letter !== true).map(letter => {
            if (aAlphabet.includes(letter)) {
                throw "To many upper case letters!";
            } else {
                return letter;
            }
        })
    } catch (e) {
        console.log(e);
        return "NO";
    }
    console.log(a.join(""));
    return "YES";

}

function abbreviation(a, b) {

    const aAlphabet = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase();
    const aInit = a;
    a = a.split("");
    b = b.split("");
    let uIndex
    let lIndex;
    let indexArray = [];
    try {
        b.forEach((letter, index) => {
            // Upper case exist
            uIndex = a.indexOf(letter);
            if (uIndex > -1) {
                indexArray[uIndex] = letter;
                a[uIndex] = 0;
                return uIndex;
            }

            // Lower case exist
            lIndex = a.indexOf(letter.toLowerCase());
            if (lIndex > -1) {
                indexArray[lIndex] = letter;
                // console.log(`${letter} === ${a[lIndex]} => ${letter === a[lIndex]}`);
                a[lIndex] = 0;
                return lIndex;
            }

            // If not break and return NOOOOOOOOOOOO!
            throw `No match for ${letter}!`;

        });

        a = a.filter(letter => letter !== true).map(letter => {
            if (aAlphabet.includes(letter)) {
                throw "To many upper case letters!";
            } else {
                return letter;
            }
        })

    } catch (e) {
        console.log(e);
        return "NO";
    }

    // console.log(a);
    // let newA = indexArray.map((value, index) => {
    //     return aInit[value];
    // });
    a = indexArray.filter(n => n).join("");
    b = b.join("");
    for (let i = 0; i < b.length; i++) {
        if (a[i] !== b[i]) {
            console.log(a.slice(0, i + 1), b.slice(0, i + 1));
            break;
        }
    }
    // console.log(indexArray.filter(n => n).join(""), b.join(""));
    // console.log(b.join("") === indexArray.join(""));
    return b === a ? "YES" : "NO";
    // return "YES";

}
const aAlphabet = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase();
function abbreviationDP2(a, b) {

    // To avoid begging case
    let aNew = '';
    a = ['', ...a.split(""), ''];
    try {
        for (let i = 0, j = 0; i < a.length - 1; i++) {

            if (a[i] === '') {
                continue;
            }

            if (a[i].isUpperCase()) {
                // Upper case
                if (a[i] === b[j]) {
                    // If you not found a match before try
                    a[i] = a[i - 1] + a[i];
                    j++;
                    continue;
                } else {
                    throw `No matching Upper letter ${a[i]} in ${b}`;

                }
            } else if (a[i].toUpperCase() === b[j]) {
                // console.log(a[i]);
                a[i] = a[i - 1] + a[i];
                j++;
            } else {
                a[i] = a[i - 1];
            }
            // console.log('%s| %i', b[j], j);
            console.log(a);
        }
    } catch (e) {
        console.log(e);
        return "NO";
    }

    // console.log(a[a.length - 2].toUpperCase());
    // if (a[a.length - 2].toUpperCase() === b) {
    if (isCorrect(a[a.length - 2], b)) {
        return "YES";
    } else {
        return "NO";
    }
}
function abbreviationDP(a, b) {

    // let numberOfULetters = a.split("").filter(val => aAlphabet.includes(val)).reduce(acc => acc += 1, 0);
    // console.log(numberOfULetters);
    // if (!b || b === '') {
    //     return "NO";
    // }
    // To avoid begging case
    a = ['', ...a.split(""), ''];
    // b = b.split("");
    try {
        for (let i = 0, j = 0; i < a.length - 1; i++) {

            if (a[i] === '') {
                continue;
            }

            if (a[i].isUpperCase()) {
                // Upper case

                if (a[i] === b[j]) {
                    // If you not found a match before try
                    a[i] = a[i - 1] + a[i];
                    j++;
                    continue;
                } else {
                    let lastValue = a[i - 1].split("");
                    let lCharReplacementIndex = lastValue.indexOf(a[i].toLowerCase());

                    // If you found a match for a letter before and now you have an upper case that better fits and can't be removed
                    if (lCharReplacementIndex > -1) {
                        // console.log(`Replacing inner index of ${lastValue} at ${lCharReplacementIndex}`);
                        lastValue[lCharReplacementIndex] = a[i];
                        a[i] = lastValue.join("");
                        console.log(lastValue, a[i]);
                        console.log(a);
                    } else {
                        throw `No matching Upper letter ${a[i]} in ${b}`;
                    }
                }
            } else if (a[i].toUpperCase() === b[j]) {
                // console.log(a[i]);
                a[i] = a[i - 1] + a[i];
                j++;
            } else {
                a[i] = a[i - 1];
            }
            // console.log('%s| %i', b[j], j);
            // console.log(a);
        }
    } catch (e) {
        console.log(e);
        return "NO";
    }

    // console.log(a[a.length - 2].split("").filter(val => aAlphabet.includes(val)).reduce(acc => acc += 1, 0), numberOfULetters);
    // console.log(a[a.length - 2].toUpperCase());
    // if (a[a.length - 2].toUpperCase() === b) {
    if (isCorrect(a[a.length - 2], b)) {
        return "YES";
    } else {
        return "NO";
    }
}

function isCorrect(a, b) {
    const consoleText = `Final comparison => ${a}, ${b}`;
    console.log("\x1b[33m", "\n", consoleText, "\n");
    if (a.toUpperCase() === b) {
        return true;
    } else {
        return false;
    }
}

String.prototype.isUpperCase = function () {
    if (this === this.toUpperCase()) {
        return true;
    } else {
        return false;
    }
}
let testFunction = abbreviationDP;

describe("Abbreviation:", function () {

    const tests = [{
    //     variables: ['daBcd', 'ABC'],
    //     expected: "YES"
    // }, {
    //     variables: ['AfPZN', 'APZNC'],
    //     expected: "NO"
    // }, {
        variables: ['ababbaAbAB', 'AABABB'],
        expected: "NO"
    // }, {
    //     variables: ['SRTRING', 'STRING'],
    //     expected: "NO"
    // }, {
    //     variables: ['erReReee', 'RRREEE'],
    //     expected: "YES"
        // }, {
        //     variables: ['rReRRREreEreERR', 'RRRREERER'],
        //     expected: "YES"
        // }, {
        //     variables: ['AfPZN', ''],
        //     expected: "NO"
        // }, {
        //     variables: ['afzfd', ''],
        //     expected: "YES"
        // }, {
        //     variables: ['aaAA', 'AA'],
        //     expected: "YES"
        //     }, {
        //         variables: ['RDWPJPAMKGRIWAPBZSYWALDBLDOFLWIQPMPLEMCJXKAENTLVYMSJNRJAQQPWAGVcGOHEWQYZDJRAXZOYDMNZJVUSJGKKKSYNCSFWKVNHOGVYULALKEBUNZHERDDOFCYWBUCJGbvqlddfazmmohcewjg',
        //             'RDPJPAMKGRIWAPBZSYWALDBLOFWIQPMPLEMCJXKAENTLVYMJNRJAQQPWAGVGOHEWQYZDJRAXZOYDMNZJVUSJGKKKSYNCSFWKVNHOGVYULALKEBUNZHERDOFCYWBUCJG'],
        //         expected: "NO"
        // }, {
        //     variables: ['rReRRREreEreERRrreeeRrrrErReerreererEreEEseeEeErreEEereeerrerREreeeeerreeerrEEEReErrEeeeeREesrRerereRrreRreRRrreeEeEeERerrreweRrrEREEerRrrreRRrrEREreEerrrerrRerReeeerrErrreREreerrrRrreeereEseErreerrEreererRreereerrreeRrreEreerreRRErRERereEEerReReeEERrEEeeEeEeereeReeeeeReEerEREReseereRereEeeerEreEEereerEeEesrerrRerrererrerrReERrreeereeeeRerrEeeEerreRRrrRErseeErrEeeeerreeRErrRrRerrrrrerRErrerEeeeerrreerrreErrerEeeeeRRererrEReEeeererErErErRerrerErRrrRRrerrErrerrreErerrrreerreERReRerererErreRrererreRrReEERRereeeErEreeREEeeeErReRrreerRrRrreeRrRrEEEEereeerErrrerreErErrRRrreErReReRerrrerEereRreerererReERREeeeeeEeRerRerReeSrreesreeeeREeErresreeReeRrerrrrererrrrrreerrrrrrRREEerrerrErRRRereeerrREEreeEeerrEeeereeerReRerrrEEerrEEReEeerErerRrErSerErRRreERrerEeeerereEreEeerrREEEReereeRErerRrrrReeReEERrrerereereeErEEREeRSreRrRrreerrrReErReErerreerrrRrseererrerererrrreEeeRReRrerreeRerrRRerRPeeRerrreeRrrREereeEErererrRRRererrrerEEerrRrrReerRereeEerrsREEReEerEreEErrrsErreErereeerrrrRrrEeeErReEeReEeeeeEe',
        //         'RRRREERERREERRERREEEEEEEEEERRREREEERRREEEEEEREESRRRRRRREEERRRREERRERREREEREREEERERRREERRRRESERERRERRRREEEEERREREREEEERREEERREEEEEEREEEREEERERERREEREREEEEEERRREREERRREERRRRRREEREEEERERREREREERRREEREREERREREREEREERERERRRERERERERRRREERRRERERRRERRREERREEEEEREREEREEEERRRREERRRRERRREEEERERERERRRERRREEREEREERERREEEERRRESRESEREESRERRRRRRRERRRREEERERRRRERREEEEEEEEERRRRRREREEREEEERRESRRRRRREREEEEREERREEERRERRRREEERREEEEEERRSRRRERRERREERREERRRREERRRRRRRRRRPRRREEEEEEERRREEERRRRRERREEREEEEEREREEEERREEEREEREE'],
        //     expected: "NO"
        //     }, {
        //         variables: ['RUJNEGMMMEGIRGILRHKWKSNZWMQAFKISNVVBOVNZBHRITDHZIKHXuZRRJOVNHIKLBIZTTHQCDRDDPQIWIJRAKXSAFKNZQQTUCGYBKKIFJBKYDLICJZZCDSHRCKRNXTNZAKNNFPLCLBMJJGOZLIIJYFIMYHPNHLXGZICXOCDNWKKEMGOSJUGVXIEGBWLNGXUQNBWKJIUURRBZYBKEVUSDUpAUQKVANNJWNJZZAIJCYTJPUMIYAFJKBBCEDOGWVUCTBRhHXTTZDFTPYTypxornxsclmxzsuwaqlsjwpztodbwnowpplxcvbpubodwobdlwphmcyenwdjwdzwblrejfhvoprxsiekxz',
        //             'RUJNEGMMMEGIRGILRHKWKSNZWMQAFKISNVVBOVNZBHRITDHZIKHXZRRJOVNHIKLBIZTTHQCDRDDPQIWIJRAKXSAFKNZQQTUCGYBKKIFJBKYDLICJZZCDSHRCKRNXTNZAKNNFPLCLBMJJGOZLIIJYFIMYHPNHLXGZICXOCDNWKKEMGOSJUGVXIEGBWLNGXUQNBWKJIUURRBZYBKEVUSDUAUQKVANNJWNJZZAIJCYTJPUMIYAFJKBBCEDOGWVUCTBRHXTTZDFTPYT'],
        //         expected: "YES"
        //     }, {
        //         variables: ['AbCdE', 'AFE'],
        //         expected: "NO"
        //     }, {
        //         variables: ['beFgH', 'EFG'],
        //         expected: "NO"
        //     }, {
        //         variables: ['beFgH', 'EFH'],
        //         expected: "YES"
    }];

    tests.forEach(test => {
        it("Test variables: " + test.variables, function () {
            assert.equal(testFunction(...test.variables), test.expected);
        })
    });
});

// describe('Fetching test data and results \n', async function () {
//     const aURL = {
//         variables: 'https://hr-testcases-us-east-1.s3.amazonaws.com/15427/input13.txt?AWSAccessKeyId=AKIAJ4WZFDFQTZRGO3QA&Expires=1540449043&Signature=71dokoOb7nBVNA0nf5%2FnZD1AzI0%3D&response-content-type=text%2Fplain',
//         expected: 'https://hr-testcases-us-east-1.s3.amazonaws.com/15427/output13.txt?AWSAccessKeyId=AKIAJ4WZFDFQTZRGO3QA&Expires=1540449051&Signature=omGYMbcy6jOyL2gLutfjwetB4ks%3D&response-content-type=text%2Fplain'
//     }

//     const path = 'data';
//     if (!fs.existsSync(`${path}/variables.txt`) || !fs.existsSync(`${path}/expected.txt`)) {
//         await wf.getAndSaveTestData(aURL);
//     }
//     const variables = fs.readFileSync(`${path}/variables.txt`, 'utf8').split('\n').map((variable, index, array) => {
//         if (index % 2 === 1) {
//             return [array[index - 1], variable];
//         } else {
//             return 0;
//         }
//     }).filter(variable => variable !== 0);
//     const expected = fs.readFileSync(`${path}/expected.txt`, 'utf8').split('\n');

//     // console.log(variables);
//     // console.log(expected);
//     variables.forEach((test, index) => {
//         it(`Fetching test case number: ${index}`, function () {
//             assert.equal(testFunction(...test), expected[index]);
//         });
//     });
// });