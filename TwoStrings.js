

String.prototype.newIncludes = function (search, start) {
    'use strict';
    if (typeof start != "number") {
        start = 0;
    }

    if (start + search.length > this.length) {
        return false;
    } else {
        return this.indexOf(search, start) !== -1;
    }
}

function twoStrings(s1, s2) {
    for (let letter of s1) {
        // if (s2.newIncludes(letter)) {
        //     return "YES";
        // }
        const s2Length = s2.length;
        for (let i = 0; i < s2Length -1 ; i++){
            if (s2[i] === letter){
                return "YES";
            } 
        }
        // console.log(letter);
    }
    return "NO";
}

console.log(twoStrings("hello", "world"));
console.log(twoStrings("hi", "world"));
console.log(twoStrings("", "world"));
console.log(twoStrings("", ""));
console.log(twoStrings("Some Me", " E"));