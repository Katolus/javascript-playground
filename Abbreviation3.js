'use strict';
var assert = require("assert");
var fs = require('fs');
const wf = require("./WriteToFile");

const aAlphabet = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase();
function abbreviation(a, b) {

    let localA;
    // let finalString; 
    let isAbb = false;
    for (let i = 0; i < a.length; i++) {
        if (a.length - i < b.length || isAbb) {
            // console.log(`${a.length - i < b.length} and ${isAbb})`);
            break;
        }
        localA = [];
        for (let j = i, k = 0; j < a.length; j++) {
            // console.log(a[j], b[k]);
            if (b[k] === undefined) {
                // If rest of a contains a upper case - destroy this localA
                if (a.slice(j).split("").filter(letter => aAlphabet.includes(letter)).length > 0) {
                    console.log(a.slice(j).split(""));
                    localA.push(0);
                    break;
                }
            }
            if (a[j] === a[j].toUpperCase()) {
                // Upper case
                if (a[j] === b[k]) {
                    // If you not found a match before try
                    localA.push(a[j]);
                    k++;
                } else if (a[j - 1] === a[j].toLowerCase()) {
                    localA.pop();
                    localA.push(a[j]);
                } else {
                    // console.log('Upper case break');
                    break;
                }
            }

            if (a[j].toLowerCase() === a[j]) {
                // Lower case
                if (a[j].toUpperCase() === b[k]) {
                    // console.log(a[i]);
                    localA.push(a[j]);
                    k++;
                }
            }
        }
        console.log(localA);
        if (localA.join("").toUpperCase() === b) {
            isAbb = true;
        }
    }

    if (isAbb) {
        // console.log(localA.join(""));
        return "YES";
    } else {
        // console.log(localA.join(""));
        return "NO";
    }

}
// abbreviationDP('ababbaAbAB', 'AABABB');
let testFunction = abbreviation;

// describe("Abbreviation:", function () {

//     const tests = [{
//             variables: ['aAbAb', 'ABAB'],
//             expected: "YES"
//         }, {
//             variables: ['baaBa', 'BAAA'],
//             expected: "NO"
//         }, {
//             variables: ['baaba', 'BAAA'],
//             expected: "YES"
//         }, {
//             // Sure this is good
//             variables: ['ababbaAbAB', 'AABABB'],
//             expected: "NO"
//         }, {
//             variables: ['abAAb', 'AAA'],
//             expected: "YES"
//         }, {
//             variables: ['daBcd', 'ABC'],
//             expected: "YES"
//         }, {
//             variables: ['AfPZN', 'APZNC'],
//             expected: "NO"
//         }, {
//             variables: ['SRTRING', 'STRING'],
//             expected: "NO"
//         }, {
//             variables: ['erReReee', 'RRREEE'],
//             expected: "YES"
//         }, {
//         variables: ['rReRRREreEreERR', 'RRRREERER'],
//         expected: "NO"
//         }, {
//             variables: ['AfPZN', ''],
//             expected: "NO"
//         }, {
//             variables: ['afzfd', ''],
//             expected: "YES"
//         }, {
//             variables: ['aaAA', 'AA'],
//             expected: "YES"
//         }, {
//             variables: ['RDWPJPAMKGRIWAPBZSYWALDBLDOFLWIQPMPLEMCJXKAENTLVYMSJNRJAQQPWAGVcGOHEWQYZDJRAXZOYDMNZJVUSJGKKKSYNCSFWKVNHOGVYULALKEBUNZHERDDOFCYWBUCJGbvqlddfazmmohcewjg',
//                 'RDPJPAMKGRIWAPBZSYWALDBLOFWIQPMPLEMCJXKAENTLVYMJNRJAQQPWAGVGOHEWQYZDJRAXZOYDMNZJVUSJGKKKSYNCSFWKVNHOGVYULALKEBUNZHERDOFCYWBUCJG'],
//             expected: "NO"
//         }, {
//             variables: ['rReRRREreEreERRrreeeRrrrErReerreererEreEEseeEeErreEEereeerrerREreeeeerreeerrEEEReErrEeeeeREesrRerereRrreRreRRrreeEeEeERerrreweRrrEREEerRrrreRRrrEREreEerrrerrRerReeeerrErrreREreerrrRrreeereEseErreerrEreererRreereerrreeRrreEreerreRRErRERereEEerReReeEERrEEeeEeEeereeReeeeeReEerEREReseereRereEeeerEreEEereerEeEesrerrRerrererrerrReERrreeereeeeRerrEeeEerreRRrrRErseeErrEeeeerreeRErrRrRerrrrrerRErrerEeeeerrreerrreErrerEeeeeRRererrEReEeeererErErErRerrerErRrrRRrerrErrerrreErerrrreerreERReRerererErreRrererreRrReEERRereeeErEreeREEeeeErReRrreerRrRrreeRrRrEEEEereeerErrrerreErErrRRrreErReReRerrrerEereRreerererReERREeeeeeEeRerRerReeSrreesreeeeREeErresreeReeRrerrrrererrrrrreerrrrrrRREEerrerrErRRRereeerrREEreeEeerrEeeereeerReRerrrEEerrEEReEeerErerRrErSerErRRreERrerEeeerereEreEeerrREEEReereeRErerRrrrReeReEERrrerereereeErEEREeRSreRrRrreerrrReErReErerreerrrRrseererrerererrrreEeeRReRrerreeRerrRRerRPeeRerrreeRrrREereeEErererrRRRererrrerEEerrRrrReerRereeEerrsREEReEerEreEErrrsErreErereeerrrrRrrEeeErReEeReEeeeeEe',
//                 'RRRREERERREERRERREEEEEEEEEERRREREEERRREEEEEEREESRRRRRRREEERRRREERRERREREEREREEERERRREERRRRESERERRERRRREEEEERREREREEEERREEERREEEEEEREEEREEERERERREEREREEEEEERRREREERRREERRRRRREEREEEERERREREREERRREEREREERREREREEREERERERRRERERERERRRREERRRERERRRERRREERREEEEEREREEREEEERRRREERRRRERRREEEERERERERRRERRREEREEREERERREEEERRRESRESEREESRERRRRRRRERRRREEERERRRRERREEEEEEEEERRRRRREREEREEEERRESRRRRRREREEEEREERREEERRERRRREEERREEEEEERRSRRRERRERREERREERRRREERRRRRRRRRRPRRREEEEEEERRREEERRRRRERREEREEEEEREREEEERREEEREEREE'],
//             expected: "NO"
//     }, {
//         variables: ['RUJNEGMMMEGIRGILRHKWKSNZWMQAFKISNVVBOVNZBHRITDHZIKHXuZRRJOVNHIKLBIZTTHQCDRDDPQIWIJRAKXSAFKNZQQTUCGYBKKIFJBKYDLICJZZCDSHRCKRNXTNZAKNNFPLCLBMJJGOZLIIJYFIMYHPNHLXGZICXOCDNWKKEMGOSJUGVXIEGBWLNGXUQNBWKJIUURRBZYBKEVUSDUpAUQKVANNJWNJZZAIJCYTJPUMIYAFJKBBCEDOGWVUCTBRhHXTTZDFTPYTypxornxsclmxzsuwaqlsjwpztodbwnowpplxcvbpubodwobdlwphmcyenwdjwdzwblrejfhvoprxsiekxz',
//             'RUJNEGMMMEGIRGILRHKWKSNZWMQAFKISNVVBOVNZBHRITDHZIKHXZRRJOVNHIKLBIZTTHQCDRDDPQIWIJRAKXSAFKNZQQTUCGYBKKIFJBKYDLICJZZCDSHRCKRNXTNZAKNNFPLCLBMJJGOZLIIJYFIMYHPNHLXGZICXOCDNWKKEMGOSJUGVXIEGBWLNGXUQNBWKJIUURRBZYBKEVUSDUAUQKVANNJWNJZZAIJCYTJPUMIYAFJKBBCEDOGWVUCTBRHXTTZDFTPYT'],
//         expected: "YES"
//         }, {
//             variables: ['AbCdE', 'AFE'],
//             expected: "NO"
//         }, {
//             variables: ['beFgH', 'EFG'],
//             expected: "NO"
//         }, {
//             variables: ['beFgH', 'EFH'],
//             expected: "YES"
//         }, {
//             variables: ['babaABbbAb', 'ABAA'],
//             expected: "NO"
//     }];

//     tests.forEach(test => {
//         it("Test variables: " + test.variables, function () {
//             assert.equal(testFunction(...test.variables), test.expected);
//         })
//     });
// });

describe('Fetching test data and results \n', async function () {
    const aURL = {
        variables: 'https://hr-testcases-us-east-1.s3.amazonaws.com/15427/input13.txt?AWSAccessKeyId=AKIAJ4WZFDFQTZRGO3QA&Expires=1540449043&Signature=71dokoOb7nBVNA0nf5%2FnZD1AzI0%3D&response-content-type=text%2Fplain',
        expected: 'https://hr-testcases-us-east-1.s3.amazonaws.com/15427/output13.txt?AWSAccessKeyId=AKIAJ4WZFDFQTZRGO3QA&Expires=1540449051&Signature=omGYMbcy6jOyL2gLutfjwetB4ks%3D&response-content-type=text%2Fplain'
    }

    const path = 'data';
    if (!fs.existsSync(`${path}/variables.txt`) || !fs.existsSync(`${path}/expected.txt`)) {
        await wf.getAndSaveTestData(aURL);
    }
    const variables = fs.readFileSync(`${path}/variables.txt`, 'utf8').split('\n').map((variable, index, array) => {
        if (index % 2 === 1) {
            return [array[index - 1], variable];
        } else {
            return 0;
        }
    }).filter(variable => variable !== 0);
    const expected = fs.readFileSync(`${path}/expected.txt`, 'utf8').split('\n');

    // console.log(variables);
    // console.log(expected);
    variables.forEach((test, index) => {
        it(`Fetching test case number: ${index}`, function () {
            assert.equal(testFunction(...test), expected[index]);
        });
    });
});