Array.prototype.myIndexOf = function (char, arrayOfExcludedIndexes) {
    for (let i = 0; i < this.length; i++) {
        if (arrayOfExcludedIndexes.indexOf(i) > -1) {
            continue;
        }
        if (this[i] === char) {
            return i;
        }
    }
    return -1;
}

function compareValuesInTwoArrays(a1, a2) {
    if (a1.length !== a2.length) {
        return false;
    }

    let foundIndexes = [];
    let currentIndex;
    for (let i = 0; i < a1.length; i++) {

        currentIndex = a2.myIndexOf(a1[i], foundIndexes);

        if (currentIndex > -1) {
            foundIndexes.push(currentIndex);
        } else {
            return false;
        }

    }
    return true;
}

console.log(compareValuesInTwoArrays([1, 2], [2, 2]));
console.log(compareValuesInTwoArrays([1, 2, 3, 5], [2, 1, 5, 3]));
console.log(compareValuesInTwoArrays([1, 2, 3, 5], [2, 1, 5, 2]));